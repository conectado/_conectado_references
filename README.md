## Conectado References

Repositório dedicado a manter arquivos de referência com exemplo práticos de uso de funções do Conectado utilizadas em scripts JavaScript.

## Sugestão de Uso do Visual Studio Code (IDE) e bitbucket.org como Repositório Git.

Para configuração seguir os seguintes passos.

1. Abrir o VSCode e fechar qualquer workspace aberta, e retirar diretórios que tenham sido adicionados à Workspace anteriormente para deixá-la totalmente vazia.
   ![](images/VSCode01_NoWorkspace.png)

2. Salvar a Workspace vazia com o Nome "Conectado" em um diretório de sua escolha em sua máquina para manter os repositórios a serem clonados do bitbucket.
   ![](images/VSCode02_SaveWorkspaceConectado.png)

3. Instalar a extensão "Jira and Bitbucket(Official)"
   ![](images/VSCode03_Install_Bitbucket_Extension.png)

4. Mostrar na barra de status a extensão Jira Bitbucket.
   ![](images/VSCode04_Show_BitBucket_Extension.png)

5. Clicar no área do "Sign in" na barra de status para abrir os "Settings" da extensão para fazer o login no Bitbucket.
   ![](images/VSCode05_SignIn_BitBucket_Extension.png)

6. No processo de "Login ro Bitbucket Cloud" este fará uma chamada ao navegador padrão para que se faça o signin no serviço. Para facilitar antes de clicar no botão de login no VSCode, pode-se logar na interface web com seu usuário no Bitbucket e assim a conexão fica facilitada.
   ![](images/VSCode06_Login_BitBucket_Extension.png)

7. Após estar conectado no Bitbucket com o Visual Code, vamos clicar no logo da Atlassian na barra esquerda das ferramentas e selecionar a opção "Clone a repository from Bitbucket.." para de clonar o repositório de Referências do Conectado.
   ![](images/VSCode07_Clone_a_Repository_from_Bitbucket.png)

8. Na Url solicitada devemos informar o link "https://ethomasini@bitbucket.org/conectado/\_conectado_references.git"

9. Após informar o link, no momento de clonar poderá aparecer perguntado se deseja abrir o repositório a clonar ou adicioná-lo na Workspace. Escolhemos, "Add to Workspace".
   ![](images/VSCode08_Add_to_Workspace.png)

10. Ao finalizar os passos anteriores no árvore de navegação do Explorer da Workspace conseguiremos ver
    ![](images/VSCode09-Repository_Cloned.png)

## Outra forma de Clonar o Repositório Conectado-References na sua Máquina Local

1. Clicar no seguinte link para integrar o repositório à sua instalação ao Visual Studio Code abra o seguinte link no sue navegador internet. "vscode://atlassian.atlascode/cloneRepository?q=https%3A%2F%2Fethomasini%40bitbucket.org%2Fconectado%2F_conectado_references.git&source=bitbucket"

## Site com tutorial sobre o Git com o Bitbucket Cloud

[Learn Git with Bitbucket Cloud](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud)
