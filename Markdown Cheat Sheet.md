# **_Elements - Markdown Syntax_**

**Heading**

# # H1

## ## H2

### ### H3

---

**Bold**

\*\***bold text**\*\*

---

**Italic**
Italic

\_ _italicized text_\_

---

**Blockquote**

> \> blockquote

---

**Ordered List**

1. First item
2. Second item
3. Third item

---

**Unordered List**

- First item
- Second item
- Third item

---

**Code**

\``code`\`

---

**Horizontal Rule**

\-\-\-

---

**Link**

\[Link Description\]\(http://...)

[Link Description](https://bitbucket.org/conectado/_conectado_references)

---

**Image**

\!\[Alternative Text\]\(relative_path/image.png\)
![Alternative Text](images/Logo%20Conectado.png)

---

For further references [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)
