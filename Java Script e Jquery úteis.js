//Todo SetValue 
    //Limpar Campo Select 2
    SetValue(alias, null)
    $('#'+alias+' option').remove();
    $('#'+alias).empty()

    //Setar valor em campo Checkbox ativando change dele
    $('#'+alias).prop('checked', true);
    SetValue(alias, true);

    $('#'+alias).prop('checked', false);
    SetValue(alias, false);
    
    //! Workaround
        //Operações com Decimais
        var valor = GetValue(alias)
        valor = valor.replace(/\./g,'').replace(',','.'); // Regex /\./g remove todos os pontos
        //realizar operações
        valor = valor.toString().replace('.',',')
        SetValue(alias, valor)

//Todo SetReadOnly
    
    //! Workarounds
        //Readonly para campo combobox (select2)*/
            $('#'+alias).attr('disabled',boolean);
            
        //Readonly para campo datetime (remove o datepicket)*/
            $('#'+alias).prop('readonly',boolean).datepicker("remove");
            
            //re-habilita o datepicker
            $('#'+alias).prop('readonly',boolean).datepicker("enable");

//Todo OnChange

    //! Workarounds
        //usando jquery
        $('#'+alias).change(function() {
            //script here
        });

        //para Radio Button
        $('input:radio[name='+alias+']').change(function() {
            //script here
        });

        //para DateTime
        $('#'+alias).blur(function(){
            //script here
        });

        //para Arquivo
        $('#'+alias+'tableAnexos').bind("DOMSubtreeModified", function() {
            //script here
        });

//Todo Click
    // Click em botão com jquery
    $("#"+alias).click(function() {
        //script here
    });

//Todo GetEntity  
    //! Workarounds
    //Pegar valor de Select2, não Id sem precisar ir no banco 
    $('#select2-'+alias+'-container').prop('title')


//Todo InputMask Jquery
    //Mascara de Decimal
    $('#'+alias).inputmask({
        'alias': 'numeric',
        'groupSeparator': '.',
        'autoGroup': true,
        'digits': 2,
        'radixPoint': ",",
        'digitsOptional': false,
        'allowMinus': false,
        'prefix': '',
        'placeholder': '',
        rightAlign: false
    });

    //Máscara Numeros*/
    $("input[id*="+alias+"]").inputmask({
    mask: '9',
    repeat: '*'
    });

    //Mascara para hh:mm para web e app
    $("#"+alias).prop("type", "time");


    // Mascara para hhh:mm
    $("#"+alias).inputmask("999:99",{numericInput:true, placeholder:"0"});

    //máscara para telefone*/
    $('#'+alias).inputmask({
        mask: ["(99) 9999-9999", "(99) 99999-9999"]
    });

    //mascara de email*/
    $('#' + alias).inputmask("email"); 

    //Máscara CPF/CNPJ  */
    // Ajustada para quando omitirem os caracteres da esquerda preencher com zeros 
    // também usa o place holder como zero e o preechimento se dá da direita para a esquerda. 
    $('input[id*='+alias+']').inputmask(
        { mask: ['999.999.999-99', '99.999.999/9999-99'], keepStatic: true, numericInput:true, placeholder:'0' }
        );
    
    //Máscara CEP*/
    // Ajustada para quando omitirem os caracteres da esquerda preencher com zeros
    // também usa o place holder como zero e o preechimento se dá da direita para a esquerda.
    $("#"+alias).mask("99999-999");

    //outra opção para CEP
    $("#"+alias).inputmask({
        mask: ['99999-999'],
        keepStatic: true, numericInput:true, placeholder:'0'
    });

    //Mascara que impede sinal negativo em campo int*/
    $("#"+alias).attr('min','0');
    $("#"+alias).attr("oninput","validity.valid||(value='')");

    //Mascara Numérica*/
    $("#"+alias).mask("99999");

    //Mascara de CNH*/
    $("#"+alias).mask("99999999999");

    //Mascara de Placa - Aceita modelo brasileiro e mercosul
    $("#"+alias).inputmask(
        {mask: ["AAA-9999", "AAA-9A99"]}
    );

    //Definir casas decimais fixas em números
    var valor
    valor.toFixed(numero_casas)

    //clockpicker 
    $('#'+alias).clockpicker({donetext: 'Ok', autoselect: 'true'})
    $('#'+alias).clockpicker('remove')

//Todo Autocomplete Remove
    //útil para campos de data, evitando que o autocomplete abra sobre o calendário
    $("#"+alias).autocomplete({
        disabled: true
    });

    //Opção 2
    if (jQuery(alias).data('autocomplete')) {
        jQuery(alias).autocomplete("destroy");
        jQuery(alias).removeData('autocomplete');
    }

//Todo DateAddDays        
    //Somar dias em data usando calendario e horário da organização
    //Espera data no formato 'yyyy-MM-dd HH:mm:ss.fff'
    DateAddDays(data,dias,usa_calendario);
    //data = data no formato 'yyyy-MM-dd HH:mm:ss.fff'
    //dias = dias (inteiro) a serem somados ou diminuidos. Para diminuir passar com sinal negativo (-).
    //usa_calendario = true ou false. Determina se deve considerar o calendario e os horários da organização.

        //Ex.: 
        var ret = DateAddDays('2018-10-31 17:50:20.437', 1,true); //considera  calendario e horario da empresa
        //Ex.: 
        var ret = DateAddDays('2018-10-31 17:50:20.437', 1,false); //não considera  calendario ou horario da empresa
        
    //! Workaround
        //Ex. Tratando data 
        var data = GetDate().split("/");
        var data_string = data[2] +'-'+ data[1] +'-'+ data[0];
        var dataSomada = DateAddDays(data_string,30,false);
        var data_split = dataSomada.substring(0, 10).split("-");
        var dataString = data_split[2] +'/'+ data_split[1] +'/'+ data_split[0];
        SetValue(alias,dataString)

//Todo SetMinMaxDateOfCalendar
    //DatePicker Max e Min Date
    SetMinMaxDateOfCalendar(alias,'minDate','maxDate','default');
    //alias = é o campo data do formulário,
    //'minDate' = data mínima admitida. Pode ser fixa no formato "YYYY-MM-DD' ou um inteiro  -1 (hoje+1), 0 (dia de hoje), 1 (hoje -1)
    //'maxDate' = data máxima permitida. Pode ser fixa no formato "YYYY-MM-DD' ou um inteiro  -1 (hoje+1), 0 (dia de hoje), 1 (hoje -1)
    //'default' = data default a fixa ao abrir o datapicker que deve er em formato string "DD/MM/YYYY"

    //Ex.:
        //Setando apenas a data Maxima 								
        SetMinMaxDateOfCalendar(alias,null,3,null);
        
        //Setando apenas a data Minima 								
        SetMinMaxDateOfCalendar(alias,2,null,null);
        
        //Setando Maxima e Minima 								
        SetMinMaxDateOfCalendar(alias,2,3,null);
        
        //Informando uma data base 						
        SetMinMaxDateOfCalendar(alias,2,3,"10/06/2019");
        
        //Outros exemplos
        SetMinMaxDateOfCalendar(alias,"2019-06-11","2019-06-30",null);
        
        SetMinMaxDateOfCalendar(alias,2,"2019-06-30",null);
        
        SetMinMaxDateOfCalendar(alias,null,"2019-06-30",null);

    //! Workaround
    //define data minima calendario = hoje
    //fix para valor "0" que não funciona como data minima para setar hoje
    let l_data_split = GetDate().substring(0, 10).split("/"); // GetDate() retorna formato dd/MM/yyyy
    let l_dataHoje = l_data_split[2] +'-'+ l_data_split[1] +'-'+ l_data_split[0]; // monta formato yyyy-MM-dd
    SetMinMaxDateOfCalendar(alias,l_dataHoje,null,null);


//Todo Remover Teclado Virtual no Smartphone com DatePicker
$('#'+dataRegistroAtendimento).datepicker({
    format: 'DD/MM/YYYY',
    autoclose: true,
    disableTouchKeyboard: true,
    Readonly: true
}).attr('readonly','readonly').css('background','#fff')

//*Todo: Operações com HH:MM (hora:minuto)*/

    


    // Calcula diferença entre duas horas minuto
    function f_calculaDiferencaHoraMinuto(horaMinIni,horaMinFim){
    
        function pad(n, len) {
            return (new Array(len + 1).join('0') + n).slice(-len);
        }
        
        function ConverteHoraMinutoMinuto(horamin){
            var hora = horamin.substring(0,2);
            var minuto = horamin.substring(3,5);
            var minutos = parseInt(hora)*60 + parseInt(minuto);
            return minutos;
        }
    
        function f_converteMinutoHoraMinuto(min){
            min = Math.trunc(min)
            const c_minuto = Math.abs(min % 60);
            const c_horas = min / 60;
            const c_hora = c_horas | 0;
            const c_hms = pad(c_hora,2)+":"+pad(c_minuto,2);
            
            return c_hms;
        }
    
        const c_minIni = ConverteHoraMinutoMinuto(horaMinIni)
        const c_minFim = ConverteHoraMinutoMinuto(horaMinFim)
        const c_minTotal = c_minFim - c_minIni
        const c_horaTotal = f_converteMinutoHoraMinuto(c_minTotal)
    
        return c_horaTotal
    }

    //Todo: Operações com Botões de Atividade
    //Esconder botões de atividades*/
    //Botão Next (Concluir Atividade)
		$('#btnFormNext').show();
		$('#btnFormNext').hide();
	//Botão Save (Salvar Rascunho)
		$('#btnFormSave').show();
		$('#btnFormSave').hide();

	//Alterar nome dos botões de ação do formulário*/
	$('#btnFormNext').text('Label Aqui').prepend('<span class="fa fa-arrow-right">&ensp;</span>')
    $('#btnFormSave').text('Label Aqui').prepend('<span class="glyphicon glyphicon-floppy-disk">&nbsp;</span>')
    $('#btnFormClose').text('Label Aqui').prepend('<span class="fa fa-remove">&nbsp;</span>')

    //Chamar Validações e Teste de Campo Obrigatório no Save
    $("#btnFormSave").prop('onclick', null); 
    $("#btnFormSave" ).click(function() {
        var model = GerarDadosSalvar();
        if (ValidarCamposObrigatorios(model.formularioJson.elementos) == true && AplicarRegrasSaidaFormularios() == true) {       
            Salvar();
        }
    });


	//Trocar coress dos botões com jquery css
	//troca cor label
	$("#botao2").css('color', '#ff1a1a');
	//troca cor fundo
	$("#botao2").css('background', '#ff1a1a');
	// padrão conectado azul escuro
		#337AB7
	// padrão conectado azul claro
		#5BC0DE
	// padrão conectado vermelho
		#D7263D
	// padrão conectado amarelo
		#F3D34A
	// padrão conectado cinza
		#7E7E7E
	// padrão conectado verde
		#00A896
	// padrão conectado laranja
		#F0AD4E
	// padrão conectado laranja escuro
		#FF6A35
	// padrão conectado fonte preta
		#333333
	// padrão branco
		#FFFFFF
	// padrão cinza campo não editavel
		#EEEEEE
	// padrão cinza texto campo não editável
		#555555

//Todo: CSSS e HTML com Jquery
    //destaque em panel
    $('#'+panelConsultaAprovacao+' > .panel-default').css({
        'border-color': '#7E7E7E',
        'box-shadow': '0px 0px 5px 5px #7E7E7E',
    })


    //Ajusta altura do Heading do Panel para ficar com mesma altura da linha de um campo comum
    $('#'+panelContato+' .panel-heading').css({
        'padding-top': '8px',
        'padding-bottom': '9px',
        'height':'32px',
    })

    $('#'+panelContato).css({
        'margin-bottom':'-17px'
    })

    
    

    //Adiciona glyphicons jquery
    
    $(campo).append('<span>&nbsp;</span><span style= "color: #D7263D; font-size:16px" class="glyphicon glyphicon-exclamation-sign">')
    $(this).find('td').eq(12).append('<span>&nbsp;</span><span style= "color: #D7263D; focssnt-size:16px" class="glyphicon glyphicon-exclamation-sign">')

    //icon gerado com id para permitir remoção
    $('#Label'+item).append('<span>&nbsp;</span><span id="exclamationIcon" style= "color: #D7263D; font-size:16px" class="glyphicon glyphicon-exclamation-sign">')
    //remove icon
    $('#Label'+item+' #exclamationIcon').remove()
    

        //Adiciona texto html em Alerta(alert) do Conectado, podendo transformar em link externo
        $('#'+alertOrcamentoDentro).empty("")
        $('#'+alertOrcamentoDentro).append(`<span class="glyphicon glyphicon-exclamation-sign"><span> Gasto <strong>${c_percentGasto}%</strong> do Orçamento.</span>`)
    
        $('#'+alertFluxodeAtualizacao).append('<span style= "font-weight: bolder;"><a href="'+l_url+'/FormularioDinamico/FormularioDinamico/Resumo?codInstancia='+c_fluxoAtualizacaoPreco+'" target="_blank">'+c_fluxoAtualizacaoPreco+'.</a></span>')

    // Colocar check box no meio da página (deixar com espaço em branco o label do campo)
    $('#'+devolverEquipe+'_Check').append('Devolver acompanhamento para equipe?')
    $('#'+devolverEquipe+'_Check').css({"width":"65%"})
    $('#'+devolverEquipe+'_Check').css({"text-align":"center"})


    //cabeçalho de form - campos
    function f_cabecalhoCSS(field) {
        //Fields
        $('#' + field).css('font-size', '16px');
        $('#' + field).css('padding', '3px 6px');
        $('#' + field).css('height', '20px');
        //$('#' + field).css('box-shadow', '2px 2px 5px');
        $('#' + field).css('border-radius', '5px');
        $('#' + field).css('margin-bottom', '-12px');
        $('#' + field).css('background', '#7E7E7E');
        $('#' + field).css('color', '##fff');
        //Labels
        $('#Label' + field).css('padding-left', '10px');
        //$('#Label' + field).css('text-shadow', '2px 2px 20px');
        $('#Label' + field).css('padding-right', '5px');
        $('#Label' + field).css('margin-bottom', '-12px');
    }
    
    //cabeçalho de form - layout externo e form reusável
    function f_layoutCabecalhoCSS(layoutCabecalho, formCabecalho){
        $('#Layout2' + layoutCabecalho).prop('style', 'margin-bottom: 0px');
        //* Coloca o div-principal com cor transparente
        let formDiv = $('#divConfiguracaoPrincipalReusableForms' + formCabecalho);
        $(formDiv[0].children).prop('style', 'margin-top: -20px; background-color: transparent; margin-bottom:-5px');
    }

    //Ensaio Ajuste css Geral 
    $(".row").css({
        'margin' : 0,
        'padding' : 0,
    })

    $(".panel-form-dinamico").css({
        'margin' : 0,
        'padding' : 0,
    })

    $(".col-lg-6").css({
        'margin' : 0,
        'padding' : 0,
    })

    $(".conecta-main-container").css({
        'padding-left' : 0,
        'padding-right' : 0,
    })



    function f_verificaSmartphone(){
        const c_android =  navigator.userAgent.toLowerCase().search('android') > -1
        const c_ios =  navigator.userAgent.toLowerCase().search('ios') > -1
        const c_windowsPhone =  navigator.userAgent.toLowerCase().search('windows phone') > -1
        var v_retorno = false
        if(c_android || c_ios || c_windowsPhone){
            v_retorno = true
        }
        return v_retorno
    }

    //  panel dentro de panel
    function f_cssSegundoPanel(panel){
        if(f_verificaSmartphone()){
            $('#'+panel+'_target > .panel-form-dinamico').css({
                'margin-left':'50px',
            })
        }
    }
    //layout
    function f_cssLayoutSmartphone(layout){
        if(f_verificaSmartphone()){
            $('#Layout2'+layout).css({
                'margin-left':'0px',
                'width':'330px',
            })
        }
    }

    //largura de campo
    if(f_verificaSmartphone()){
        $('#'+dataHoraInicioRegistro).css({
            'width':'160px',
        })
        $('#'+dataHoraFimRegistro).css({
            'width':'160px',
        })
        $('#'+odometroInicial).css({
            'width':'160px',
        })
        $('#'+odometroFinal).css({
            'width':'160px',
        })
    }

    // css de tabela
    function f_cssTabelaSmartphone(tabela){
        if(f_verificaSmartphone()){
            $('#'+tabela).parent().parent().css({
                'width' : $( window ).width() * 0.92,
            })
            $('#Button'+tabela).css({
                'margin-bottom': '20px',
                'margin-left': '20px',
            })
                
        }
    }

//Todo TableCountRows    
    //Conta linhas de uma tabela
    TableCountRows(alias)
    //alias = é o id ou alias da tabela no formulário,
    
    // exemplo de manipulação de cor de campos de tabela	
	if(TableCountRows('20180108104611377') > 0){         
		$('#20180108104611377 tbody tr').each(function() {
			var situacao = $(this).find('td').eq(8).html();
			situacao = situacao.toLowerCase();
			if (situacao == 'cancelada'){
				$(this).find('td').eq(8).css('background', '#D7263D');
				$(this).find('td').eq(8).css('color', '#ffffff');
			}
			if (situacao == 'em execução'){
				$(this).find('td').eq(8).css('background', '#00A896');
				$(this).find('td').eq(8).css('color', '#ffffff');
			}
			if (situacao == 'concluída'){
				$(this).find('td').eq(8).css('background', '#7E7E7E');
				$(this).find('td').eq(8).css('color', '#ffffff');
			}
		});
	}

    //iterando de baixo para cima com reverse()
    jQuery.fn.reverse = [].reverse;
    function f_defaultTecnicoResponsavel() {
        let l_tecnicoDefault
        if(GetValue(responsavelObra) == 'false') {
            $('#'+tabelaTecnicosAgendados+' tbody tr').reverse().each(function() {
                let l_responsavel = $(this).find('td').eq(6).html();
                l_responsavel = l_responsavel.toLowerCase();
                const c_tecnico = `'${$(this).find('td').eq(2).html()}'`
                if (l_responsavel == 'true' || l_responsavel === true || l_responsavel == 'sim'){
                    l_tecnicoDefault = c_tecnico
                    return false;
                }
            });
        }
    }


    //Sair do Looping Each
    return false;

    //pegar id do registro 
    $(this).closest('tr').find('td[data-key]').data('key')


//Todo Alterar icone edição de tabela
    //Contar registros de tablequery
	$('#TableQuery20200625110718105 tbody tr').length


        //mudar botão de edição para icone de consulta e ocultar manualmente por registro
        $('#'+tabelaAtendimentos+' tbody tr').each(function() {
            const c_tecnicoResponsavel = $(this).find('td').eq(12).html();
            const c_idAtendimento = $(this).closest('tr').find('td[data-key]').data('key')
            if(c_tecnicoResponsavel == 'Sim'){
                $(this).find('td').eq(0).html(`<a href="#" onclick="EditarCollectionForm('${tabelaAtendimentos}','${c_idAtendimento}'); return false; " class="glyphicon glyphicon-list-alt"></a>`)
            }else{
                $(this).find('td').eq(0).html('') //remove todos os botões
                //deixar sem else se quiser ambos os botões normais (deleção e edição)
            }
            
        });
	//Exemplo de Iteração(iterar) usando FOR em objeto de tabela*/
	function bloquearEdicao(){
		var v_user = GetUserInfo();
		var v_idUser = v_user.id;
		for(var v_n = 1; v_n <= TableCountRows(tabelaOrientacoes); v_n++){
			var obj = GetRecord(tabelaOrientacoes,v_n);
		console.log('usuario = '+obj.GetValue('CL_ORIENTACOES_CHAMADO.COD_USUARIO'))
			if(obj.GetValue('CL_ORIENTACOES_CHAMADO.COD_USUARIO') != v_idUser){
				obj.SetDeletable(false);
			}else{
				obj.SetDeletable(true);
			}
		}
	}

    //Pega conteúdo de tabela (table)*/
	function RecalculaTabela(){
		var totalKmDeslocamento = 0;
		if(HasValue('20180328155449131')){         
			$('#20180328155449131 tbody tr').each(function() {
				var kmDeslocamento = $(this).find('td').eq(3).html();
				var custoKmDeslocamento = $(this).find('td').eq(4).html();
				var precoKmDeslocamento = $(this).find('td').eq(5).html();
				totalKmDeslocamento = totalKmDeslocamento + kmDeslocamento
			});
			
			SetValue(campoTotalKmDeslocamento,totalKmDeslocamento);
		}
	}

	// Contar registros de tabela
	if($('#20180606140555525 tbody tr').length > 1){         
		SetVisible('20180606140544882',true);
	}else{
		SetVisible('20180606140544882',false);
	}

    //ocultar coluna de tabela manualmente
    $('#'+tabelaAtendimentos+' tr > *:nth-child(numero_coluna)').hide();

    //Forçar quebra de linha conforme está no campo texto (ou com CHAR(13)+CHAR(10) no SQL)
    $(this).find('td').eq(6).css('white-space','pre')

    //remove cabeçalho de uma coluna
    $('#'+tabelaLogAnaliseParecerista+' thead tr').find('th').eq(2).hide()


  	// mostrar o nome dos arquivos /
	var list = files.List();
	for(var a = 0; a< list.length;++a){
	console.log(list[a].name);
	}

    //!workaround

    //contornar DeleteAll() que não funciona
    function f_excluirAnexos(idElemento) {
        for (var u, i = RetornaElemento(JsonFormulario.elementos, idElemento), r = 0; r < i.anexos.length; ++r)
        if (u = i.anexos[r]) {
            u.tipExcluido = !0;
        }
        BindTableAnexos(i)
    }
    //Exemplo de chamada
    f_excluirAnexos('20210817180901704')

	//CÓDIGO COMPLETO PARA COPIAR ARQUIVO DE UM CAMPO PARA OUTRO
	var copiar = GetValue('20180827095026546');
	if(copiar === true){
        var Allfiles = GetFiles('20180405161333167');
        if (Allfiles!== null && Allfiles.List().length > 0){
            var files = Allfiles.List();
            var str = files[0].name;
            var nome = str.substring(0, str.indexOf("."));
            var ext = str.substring(str.indexOf("."),str.length);
            var sufix = files.length;
                Number.prototype.pad = function(size) {
            var s = String(this);
            while (s.length < (size || 2)) {s = "0" + s;}
            return s;
            };
            var novonome = nome + " #" + sufix.pad(2) + ext;
            Allfiles.Rename(files[0].name , novonome);
            Allfiles.CopyFileTo('20180405161304413', novonome);
            Allfiles.DeleteFile(novonome);
        }
        SetValue('20180827095026546',false);
	}

//*Todo: Operações com Abas*/
	//escolher/selecionar tab (aba) via JavaScript
    function f_selecionarTab(tabId){
        var v_navTab = $('#Tabs' + tabId).parent().attr('id')
        var v_href = $('#Tabs' + tabId + ' > a').attr('href')
        $('#' + v_navTab + ' a[href="' + v_href + '"]').tab('show');
    }

	//Pegar Click de aba
	$('#'+AbaReservarVeiculo).click(function() {
	});

    function f_abaNaoImplementada(objeto){
        $('#' + objeto).css({
            'border-left-color': 'unset',
            'border-left-style': 'unset',
            'border-left-width': 'unset',
            'color': '#C0C0C0'
        })
    }

    //Ex.: Chamada para várias abas
    // abas desabilitadas
    var v_abas = new Array([]);
    v_abas.push(abaHistorico,abaLogs);
    v_abas.forEach(function (item) {
        f_abaNaoImplementada(item);
    });

//*Todo: Operações com Botões*/
	// abre url  ao clicar no botão
	$("#20190805152620052_Button" ).click(function() {
		var ret = SqlQuery('SELECT TOP 1 DES_URL_AGENDA_VEICULOS Url\
							FROM CL_PARAMETROS');
		if(ret !== null && ret.entity !== null && ret.entity.length >0){
			var url = ret.entity[0].Url;
			window.open(url, '_blank');
		}
		
	});

	//informações adicionais no botão
	$("#20190805152620052_Button").mouseenter(function() {
	var txt = function() {
	$("#20190805152620052_Button").append( "<div>Usuário: powerbipia@conecta.com.br | Senha: PowerB!123.</div>" );
	};

	setTimeout(txt, 1000);
	});

	$("#20190805152620052_Button").mouseleave(function() {
	var shw = function() {
	$("#20190805152620052_Button").text("Verificar Agenda de Veículos");
	};
	setTimeout(shw, 500);
	});


	// clicar em botão
	$('#btnFormNext').trigger('click');

//*Todo: Operações com Texto*/

	//Comando para quebrar texto usando um caracter como referencia*/
	var parts1 =GetValue('20181001160601302').split('/');
	var dIni = new Date(parts1[2], parts1[1] - 1, parts1[0]);
	var parts2 = GetValue('20181001160606508').split('/');
	var dFim = new Date(parts2[2], parts2[1] - 1, parts2[0]);

	//Inserir link de resumo via JS*/
	var instancia = [/*código para retornar numero da instancia desejada*/]
	var parametro = SqlQuery('SELECT TOP 1 DES_URL_APLICACAO URL FROM CL_PARAMETROS');
	var url = parametro.entity[0].URL;
	var resumo_chamado = url+'FormularioDinamico/FormularioDinamico/Resumo?codInstancia='+instancia;

		
	//Remover espaços em branco desnecessários
	var produtoOp1 = ret.entity[0].Produto;
	produtoOp1.trim() === 'Suporte Técnico'

	// função para converter texto em boolean	
	function parseBool(val) { return val === true || val === "true" }


//*Todo: Operações com Arrays*/
    //Declaração
    var sequencias = new Array([]);
    
    //Iterar com Array
    sequencias.forEach(function (item, indice, array) {
        console.log(item, indice);
    }
    
    //Adicionar item ao final do Array
    //sequencias.push('item')
    
    //Procurar item no Array
    //sequencias.indexOf('item');
    
    //Mais em
    https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array
    //Ordenar array de datas*/
    datas.sort(function (a, b) {
        var da = new Date(a).getTime();
        var db = new Date(b).getTime();

        return da < db ? -1 : da > db ? 1 : 0
    });

    //Verificar itens duplicados em array
    function CountArrayItemDuplicated(original) {
        // faz uma copia do array original
        var copia = original.slice(0);
        // 1º loop em todos os elementos da original
        var Contagem = 0;
        for (var i = 0; i < original.length; i++) {
            // loop em cada elemento da copia para ver se tem duplicados
            for (n = 0; n < copia.length; n++) {
                //para comparar Datas usar .getTime() na data para comparar milessegundos
                if (original[i] == copia[n]) {
                    Contagem++;
                    if(Contagem > 1){
                        //  código para quando encontra cópia (Contagem > 1)
                        break;
                    }
                }
            }
        }
    }
//intersecção enre arrays
    function intersect (arrList) {
        var arrLength = Object.keys(arrList).length;
            // (Also accepts regular objects as input)
        var index = {};
        for (var i in arrList) {
            for (var j in arrList[i]) {
                var v = arrList[i][j];
                if (index[v] === undefined) index[v] = {};
                index[v][i] = true; // Mark as present in i input.
            };
        };
        var retv = [];
        for (var i in index) {
            if (Object.keys(index[i]).length == arrLength) retv.push(i);
        };
        return retv;
    };


    //TODO VALIDATION

    // quebra de linha no ValidationMessage	
    ValidationMessage('O horário de Fim da Viagem deve ser maior que Início da Viagem! \
            <br>\n Início da Viagem: '+formatDate(dIni)+'\
            <br>\n Fim da Viagem: '+formatDate(dFim));
    //Exemplo:
    if (!HasValue('1122')){
        ValidationMessage('Você deve preencher o campo '+'Nome campo'+' para continuar.')
    }

    //mover a tela para algum lugar após clicar em Ok na mensagem de validação    
    // deve ser colocado logo após o ValidationMessage() para funcionar
    $(".ajs-button" ).click(function() {
        if($('.ajs-content').text() == 'Para Finalizar o Ticket você deve inserir pelo menos um registro de Atendimento.'){
            $('html,body').animate({
                scrollTop: $("#"+panelRegistroAtendimento).offset().top /*pode ser acresido ou subtraido uma quantidade fixa de px. Exemplo: + 100 ou -100*/
            },500);
        }
    });

    //Mensagem em Html
    const c_text = `<div>Informe todos os campos obrigatórios do Atendimento. Faltou Informar: <br>\n <strong>${l_faltouInformar}</strong>.</div>`
    ValidationMessage($('<div/>').html(c_text).html())

//*Todo: Definir focus num campo*/
$('#20180402074839753').focus();

//*Todo:  Alterar Labell Campo*/
$("#Label20190906160449942").text("Km Atual: 303xxx")


//*Todo: Busca CEP*/
$("#20180328141741682" ).change(function() {
    $.getJSON("https://viacep.com.br/ws/"+ GetValue('20180328141741682') +"/json/?callback=?", function(dados) {
        if (!("erro" in dados)) {
            //Atualiza os campos com os valores da consulta.
            SetValue('20180328141545849', dados.logradouro);
            SetValue('20180328141619824',dados.bairro);
            SetValue('20180530105228031',dados.localidade);
        }else {
            //CEP pesquisado não foi encontrado.
            SetValue('20180328141545849', null);
            SetValue('20180328141619824', null);
            SetValue('20180530105228031', null);
            alert("CEP não encontrado.");
        }
    });
});

//*Todo:  Looping em JS*/
    //While Js
    while (i < 10) {
        text += "The number is " + i;
        i++;
    }
    //Cancelar while
    break

    //*Todo: Jquery para detecção de mouse*/
    //tirar  mouse do campo/botão
    $("#20190910165923034").mouseleave(function(){
        //código
        });
        // colocar o mouse sobre o campo/botão
        $("#20190910165923034").mouseenter(function(){
        //código
        });
        // clicar sobre o campo/botão
        $("#20190910165923034").click(function(){
        //código
        });
        // clicar duplamente sobre o campo/botão
        $("#20190910165923034").dblclick(function(){
        //código
        });
        
        
    //*Todo: Diversos*/
        //Mostrar Mensagem de Alerta para ler conteúdo do campo ou expressão
        alert('conteúdo');
    
    
        //Mostra a tela de aguarde*/
            loading();
                
        //fecha a tela de aguarde*/
            loaded();
        //buscar outro campo do registro selecionado
        $('#id_campo_destino').val($('#id_combobox>option:selected').data('nomecampo')).change();
        ex.: $('#desTipoCursoExib').val($('#turma_idcurso>option:selected').data('destipocurso')).change();
    
    
        //Todo: Abrir URL
            window.open( URL, name, Specs )
            Ex.: 
            $("#20180528130053775_Button" ).click(function() {
                window.open( 'http://testeacademico.itinerariosdosaber.org/miolo20/html/index.php?module=basic&action=main&authenticationHash=69ac07b07f4c0301a8c1ecf87bf79a27&return_to=http://testecompeto.itinerariosdosaber.org/matricula?turma=2540')
            });
    
    
        //capturar tecla enter para remover focus do campo
        $('#20180328134452083').keyup(function(e){
            if (e.which ==13){
                $(this).blur();
            }
        });
    
    
        //acrescentar zeros à esquerda
        Number.prototype.pad = function(size) {
                var s = String(this);
                while (s.length < (size || 2)) {s = "0" + s;}
                return s;
                }
        var str = 1
        var novonome = str.pad(2); // chamar a função
    
    
        //substring
        var str = "Hello world!";
        var res = str.substring(start, end); // começa em zero
            Ex.: var data = '2018-07-11 10:23:34.000'
                var data_hora_minuto = data.substr(0,17);
                Resultado = 2018-07-11 10:23
    
        var str = "Hello world, welcome to the universe.";
        var n = str.indexOf("welcome"); // retorna primeira posição
    
        //Todo Scroll para um elemento
        
        //scroll para elemento do form
        document.getElementById("20210817094538972").scrollIntoView(); 
    
        // scroll para ultima linha da tablea
        var rowpos = $('#20210817094548428 tr:last').position();
        $('#TBMAXDIV_20210817094548428').scrollTop(rowpos.top);
    
        //scroll to the bottom of "#myDiv"
        var myDiv = document.getElementById("20210817094548428");
        myDiv.scrollTop = myDiv.scrollHeight;
    
        //Exemplo chamada WS (webservice)
        http://conectadoteste.maiati.com.br/IntegracaoExterna/tUtSlpHRTXwmy3l1foTqI9rYp/IntegracaoAtividades/IniciarInstancia?jsonModel={"desNome": null,"desFantasia": "Processo de Suporte","desUsuarioEmail": "conecta@conecta.com.br","desVersao": null,"desEventoInicial": "Novo Chamado Portal"}
    
        http://conectadoteste2.conecta.com.br/IntegracaoExterna/nkkWy9Z6d4V1l9ggI5jtgIfSe/IntegracaoAtividades/IniciarInstancia?jsonModel={"desNome": null,"desFantasia": "Processo de Apoio","desUsuarioEmail": "conecta@conecta.com.br","desVersao": null,"desEventoInicial": "Novo Chamado ERP / Portal"}
    
        http://conectadoteste.maiati.com.br/IntegracaoExterna/tUtSlpHRTXwmy3l1foTqI9rYp/IntegracaoAtividades/IniciarInstancia?jsonModel={"desNome": null,"desFantasia": "Processo de Suporte","desUsuarioEmail": "conecta@conecta.com.br","desVersao": null,"desEventoInicial": "Novo Chamado Portal","Entidades":{"Tables":[{"name": "CL_CHAMADOS", "Rows":[{"Columns":[{"name":"idCL_CLIENTE","value":"1","businessKey": false},{"name":"idCL_CONTATO","value":"1","businessKey": false}]}]}]}}
    
    
        http://conectadoteste2.conecta.com.br/IntegracaoExterna/nkkWy9Z6d4V1l9ggI5jtgIfSe/IntegracaoAtividades/IniciarInstancia?jsonModel={"desNome": null,"desFantasia": "Processo de Apoio","desUsuarioEmail": "conecta@conecta.com.br","desVersao": null,"desEventoInicial": "Novo Chamado ERP / Portal","Entidades":{"Tables":[{"name": "CL_CHAMADOS", "Rows":[{"Columns":[{"name":"idCL_CHAMADO","value":"","businessKey": true},{"name":"idCL_CLIENTE","value":"661","businessKey": false},{"name":"idCL_CONTATO","value":"3617","businessKey": false}]}]}]}}
    
    
        // Buscar Cotação de Moedas BACEN
        var a = GetCotacaoMoeda(codigo);
        var valor = a.svalor;
    
        Informações disponiveis:
        ano
        anoFim
        bloqueado
        bloqueioLiberado
        dia
        diaFim
        mes
        mesFim
        oid
        oidSerie
        svalor
        valor     
    /*
        Códigos possíveis : 
    
        1       Dólar (venda)
        10813   Dólar (compra)
        21619   Euro (venda)
        21620   Euro (compra)
        21621   Iene (venda)
        21622   Iene (compra)
        21623   Libra esterlina (venda)
        21624   Libra esterlina (compra)
        21625   Franco Suíço (venda)
        21626   Franco Suíço (compra)
        21627   Coroa Dinamarquesa (venda)
        21628   Coroa Dinamarquesa (compra)
        21629   Coroa Norueguesa (venda)
        21630   Coroa Norueguesa (compra)
        21631   Coroa Sueca (venda)
        21632   Coroa Sueca (compra)
        21633   Dólar Australiano (venda)
        21634   Dólar Australiano (compra)
        21635   Dólar Canadense (venda)
        21636   Dólar Canadense (compra)
    */
    
    
        //CORRECAO BUG Usado para corrigir o problema no select2 no mobile
        FixBugSelect2ModalPopup()
        setTimeout(function () {
            FixBugSelect2Resize(JsonFormulario.elementos);
        }, 100);
    
        // Força atualizaçaão Assincrona da Tabela de Contatos do Cliente
        ApplyTableFilterAsync(DadosdoClienteTabelaContatosdoCliente
                            ,'idCL_CLIENTE '+ ( (XCode_idCL_Cliente===null) ? 'is null' : ' = '+ XCode_idCL_Cliente )
                            ,function(){});
    
        // Carrega Parâmetro para uso do Formulário Reutilizável "Dados do Cliente" tabela "CL_CLIENTES"
        var XCode_idCL_Cliente = GetValue(DadosExpedicaoCliente);
        OnChange(DadosExpedicaoCliente, function(){ 
            XCode_idCL_Cliente = GetValue(DadosExpedicaoCliente);
        })
        // ----------------------- //
    
    
//*Todo: Funções Elder*/

//Botão Liga Desliga em Tabela

//*--------------------------------------------------------------------------------------------------------------------------
// Função que Define cor da Célula Simulando Botão, 3o Parametro Muda MousePointer (booleam), 4° Parametro, largura da coluna
// Parametros:
//      Obrigatórios: 
//          table           = id da tabela
//          col             = numero indíce da coluna
//      Opcionais:
//          pointer         = muda MousePointer (boolean)     
//          wid             = largura desejada para a coluna
//          falseColour     = 1 (false vermelho); 2 (false cinza)
//          criterialCol1   = coluna que contém a condição 1 para aplicar o formato
//          operator1       = operador de comparação 'entre aspas' para a condição 1. Ex.: '==', '!=', '>', '<=', etc
//          criterial1      = condição 1 que será comparada com a coluna  criterialCol1
//          criterialCol2   = coluna que contém a condição 2 para aplicar o formato
//          operator2       = operador de comparação 'entre aspas' para a condição 2. Ex.: '==', '!=', '>', '<=', etc
//          criterial2      = condição 2 que será comparada com a coluna  criterialCol2
//          criterialCol3   = coluna que contém a condição 3 para aplicar o formato
//          operator3       = operador de comparação 'entre aspas' para a condição 3. Ex.: '==', '!=', '>', '<=', etc
//          criterial3      = condição 3 que será comparada com a coluna  criterialCol3
//      Exemplo de Chamada:
//          f_TableColorSimNao(tabelaId, 10)
//          f_TableColorSimNao(tabelaId, 10, '50px', false)
//          f_TableColorSimNao(tabelaId, 10, '50px', true, 3, '==', 'Teste')
//          f_TableColorSimNao(tabelaId, 10, '50px', true, 3, '==', 'Teste', 5 '!=', 'Cancelado')
//          f_TableColorSimNao(tabelaId, 10, '50px', true, 3, '==', 'Teste', 5 '!=', 'Cancelado', 7, '>=', 1000)
//*--------------------------------------------------------------------------------------------------------------------------
function f_TableColorSimNao(table, col, pointer = true, wid = 60,falseColour = 1, criterialCol1, operator1, criterial1 , criterialCol2, operator2, criterial2, criterialCol3, operator3, criterial3 ) {
    $('#' + table + ' tr').each(function () {

        var wcel = $(this).find('td').eq(col);
        wcel.css('width',  wid+'px')

        let l_criterial1 = criterialCol1 != undefined && operator1 != undefined && criterial1 != undefined ? Boolean(eval(`'${$(this).find('td').eq(criterialCol1).html()}' ${operator1} '${criterial1}'`)) : true
        let l_criterial2 = criterialCol2 != undefined && operator2 != undefined && criterial2 != undefined ? Boolean(eval(`'${$(this).find('td').eq(criterialCol2).html()}' ${operator2} '${criterial2}'`)) : true
        let l_criterial3 = criterialCol3 != undefined && operator3 != undefined && criterial3 != undefined ? Boolean(eval(`'${$(this).find('td').eq(criterialCol3).html()}' ${operator3} '${criterial3}'`)) : true

        if(l_criterial1 && l_criterial2 && l_criterial3) {
            wcel.css('border-radius', '20px');
            if (pointer) {
                wcel.css('cursor', 'pointer');
            }
            if(wcel.html() === '' ) {
                wcel.html('&nbsp;&nbsp;!')
            }
            if(wcel.html() == '&nbsp;&nbsp;'+'!'){
                wcel.css('text-align', 'left');
                wcel.css('font-weight', 'bolder');
                wcel.css('font-size', '14px');
                wcel.css('color', '#C2A93A');
                wcel.css('background-color', '#fff');
                wcel.css('box-shadow', '#CCC 5px 5px 5px 0, inset -'+(wid-37)+'px 0px 0px 0px #F3D34A');
            }else if (wcel.html() === 'Sim') {
                wcel.css('text-align', 'right');
                wcel.css('font-weight', 'bold');
                wcel.css('color', '#008779');
                wcel.css('background-color', '#fff');
                wcel.css('box-shadow', '#CCC 5px 5px 5px 0, inset '+(wid-39)+'px 0px 0px 0px #00A896');
            } else if(falseColour == 1) {
                wcel.css('text-align', 'left');
                wcel.css('font-weight', 'bold');
                wcel.css('color', '#AB1F32');
                wcel.css('background-color', '#fff');
                wcel.css('box-shadow', ' #CCC 5px 5px 5px 0, inset -'+(wid-39)+'px 0px 0px 0px #D7263D');
            }else if (falseColour == 2){
                wcel.css('text-align', 'left');
                wcel.css('font-weight', 'bold');
                wcel.css('color', '#646464');
                wcel.css('background-color', '#fff');
                wcel.css('box-shadow', ' #CCC 5px 5px 5px 0, inset -'+(wid-39)+'px 0px 0px 0px #7E7E7E');
            }
        }else {
            wcel.css('color', wcel.css('background-color'));
            return true;
        }
    });
}  



//* Função para somar coluna testando coluna de condição
function f_somaCondicional(tabela, colSoma,casasDecimais,colCondicao1,condicao1,colCondicao2,condicao2){
    var v_totalColunaSoma = 0.00
    $('#' + tabela + ' tr').each(function () {
        let l_colunaSoma =  ($(this).find('td').eq(colSoma).html());
        let l_colunaCondicao1 =  colCondicao1 == undefined ? undefined : ($(this).find('td').eq(colCondicao1).html());
        let l_colunaCondicao2 =  colCondicao2 == undefined ? undefined : ($(this).find('td').eq(colCondicao2).html());
        //console.log('l_colunaSoma = ',l_colunaSoma)
        //console.log('typeof l_colunaSoma = ',typeof(l_colunaSoma))
        if (l_colunaSoma !== undefined){
            l_colunaSoma = l_colunaSoma.replace('&nbsp;','')
        } 
        if (l_colunaCondicao1 == condicao1 && l_colunaCondicao2 == condicao2 && l_colunaSoma != 'null' && l_colunaSoma != '' && l_colunaSoma != '0,00' && l_colunaSoma !== 0 && l_colunaSoma !== undefined){
            let l_colunaSomaToFLoat = parseFloat(l_colunaSoma.replace(',','.'))
            v_totalColunaSoma += l_colunaSomaToFLoat
        } 
        
    });
    let l_totalColunaSomaToString = v_totalColunaSoma.toFixed(casasDecimais).toString().replace('.',',');
    //console.log('l_totalColunaSomaToString = '+l_totalColunaSomaToString)
    return l_totalColunaSomaToString;
}

//Exemplo de Chamada
f_somaCondicional(tabelaRegistrosTempoChamado, 5, 2, 7, 'Sim', 3, 'Deslocamento' ));

//* Função para contar tabela testando coluna de condição
function f_contaCondicional(tabela, colCondicao1,condicao1,colCondicao2,condicao2){
    var v_contaColuna = 0
    $('#' + tabela + ' tr').each(function () {
        let l_colunaCondicao1 =  colCondicao1 == undefined ? undefined : ($(this).find('td').eq(colCondicao1).html());
        let l_colunaCondicao2 =  colCondicao2 == undefined ? undefined : ($(this).find('td').eq(colCondicao2).html());
        //console.log('l_colunaSoma = ',l_colunaSoma)
        //console.log('typeof l_colunaSoma = ',typeof(l_colunaSoma))
        if (l_colunaCondicao1 == condicao1 && l_colunaCondicao2 == condicao2){
            v_contaColuna++
        } 
    });
    //console.log('l_totalColunaSomaToString = '+l_totalColunaSomaToString)
    return v_contaColuna;
}

//Exemplo de Chamada
f_contaCondicional(tabelaRegistrosTempoChamado, 7, 'Sim', 3, 'Deslocamento' ));


//* Função para somar coluna testando coluna de condição com colunas no formato hh:mm (retornar hh:mm string)
function f_somaCondicionalHoraMinuto(tabela, colSoma,colCondicao1,condicao1,colCondicao2,condicao2){
    var v_totalColunaSoma = 0

    //Transforma tempo hh:mm em minuto (Funciona apenas para formato hh:mm)*/
    function ConverteHoraMinutoMinuto(horamin){
        var hora = horamin.substring(0,2);
        var minuto = horamin.substring(3,5);
        var minutos = parseInt(hora)*60 + parseInt(minuto);
        return minutos;
    }

    $('#' + tabela + ' tr').each(function () {
        let l_colunaSoma =  ($(this).find('td').eq(colSoma).html());
        let l_colunaCondicao1 =  colCondicao1 == undefined ? undefined : ($(this).find('td').eq(colCondicao1).html());
        let l_colunaCondicao2 =  colCondicao2 == undefined ? undefined : ($(this).find('td').eq(colCondicao2).html());
        //console.log('l_colunaSoma = ',l_colunaSoma)
        //console.log('typeof l_colunaSoma = ',typeof(l_colunaSoma))

        if (l_colunaSoma !== undefined){
            l_colunaSoma = l_colunaSoma.replace('&nbsp;','')
        } 
        if (l_colunaCondicao1 == condicao1 && l_colunaCondicao2 == condicao2 && l_colunaSoma != 'null' && l_colunaSoma != '' && l_colunaSoma != '0,00' && l_colunaSoma !== 0 && l_colunaSoma !== undefined){
            v_totalColunaSoma += ConverteHoraMinutoMinuto(l_colunaSoma)
        } 


        
    });

    //Converte minuto em hh:mm
    function f_converteMinutoHoraMinuto(min){
        min = Math.trunc(min)
        const c_minuto = Math.abs(min % 60);
        const c_horas = min / 60;
        const c_hora = c_horas | 0;
        const c_hms = pad(c_hora,2)+":"+pad(c_minuto,2);
        
        return c_hms;
    }

    let l_totalColunaSomaToString = f_converteMinutoHoraMinuto(v_totalColunaSoma)
    //console.log('l_totalColunaSomaToString = '+l_totalColunaSomaToString)
    return l_totalColunaSomaToString;
}



//Todo: Exemplo Função que monta Relatório HTML

function f_geraRelatorioAtendimento(registro,dadosForm = true){

    //console.log('registro = ',registro)
    var v_relatorio = null

    const c_query = SqlQuery(`
    SELECT
        ISNULL(AAT.DES_NUMERO_ATENDIMENTO ,'') as NumeroAtendimento
        ,ISNULL(FORMAT(TC.DATA_INICIO,'dd/MM/yyyy') ,'') as DataInicio
        ,ISNULL(OST.DES_OS ,'') as NumeroOs
        ,ISNULL((SELECT TOP 1 FORMAT(RHC.DTA_HORA_INICIO,'HH:mm') 
                FROM CL_REGISTROS_HORARIO_CHAMADO RHC
                WHERE RHC.idCL_TIPO_REGISTRO_HORARIO = 2
                AND RHC.idCL_ATENDIMENTO_ASSIS_TEC = AAT.idCL_ATENDIMENTO_ASSIS_TEC) ,'') as HoraInicio
        ,ISNULL((
            SELECT U.DES_NOME_COMPLETO
            FROM PCPJ_USUARIOS U
            WHERE U.COD_USUARIO = TC.COD_TECNICO) ,'') as NomeTecnico
        ,ISNULL(CT.DES_RAZAO_SOCIAL ,'') as NomeCliente
        ,ISNULL(CT.DES_CIDADE ,'') as CidadeCliente
        ,ISNULL(CT.DES_UF ,'') as EstadoCliente
        ,ISNULL(CCT.DES_NOME ,'') as ResponsavelCliente
        ,ISNULL(CCT.DES_FUNCAO ,'') as CargoResponsavel
        ,ISNULL(CCT.DES_TELEFONE ,'') as TelefoneResponsavel
        ,ISNULL(CCT.DES_EMAIL ,'') as EmailResponsavel
        ,ISNULL(IOT.DES_TIPO_EQUIPAMENTO ,'') as TipoEquipamento
        ,ISNULL(IOT.DES_PV_PROPOSTA ,'') as PVProposta
        ,ISNULL(IOT.DES_MODELO_EQUIPAMENTO ,'') as ModeloEquipamento
        ,ISNULL(IOT.DES_NUMERO_SERIE ,'') as NroSerieEquipamento
        ,ISNULL(CAT.DES_CHAMADO ,'') as DescricaoChamado
        ,ISNULL(AAT.DES_SINTOMA ,'') as SintomaAtendimento
        ,ISNULL(AAT.DES_CAUSA ,'') as CausaAtendimento
        ,ISNULL(AAT.DES_SOLUCAO ,'') as SolucaoAtendimento
        ,ISNULL(AAT.DES_COMENTARIOS_TECNICOS ,'') as ComentariosTecnicoAtendimento
        ,CASE AAT.idCL_SITUACAO_EQUIPAMENTO WHEN 3 THEN 'X' ELSE '' END as idSituacaoEquipamento1
        ,CASE AAT.idCL_SITUACAO_EQUIPAMENTO WHEN 2 THEN 'X' ELSE '' END as idSituacaoEquipamento2
        ,CASE AAT.idCL_SITUACAO_EQUIPAMENTO WHEN 1 THEN 'X' ELSE '' END as idSituacaoEquipamento3
        ,ISNULL((
            SELECT SUM(RHC.NUM_DURACAO_HORAS)
            FROM CL_REGISTROS_HORARIO_CHAMADO RHC
            WHERE RHC.idCL_TIPO_REGISTRO_HORARIO = 1
            AND RHC.idCL_ATENDIMENTO_ASSIS_TEC = AAT.idCL_ATENDIMENTO_ASSIS_TEC) ,0) as TotalHorasViagem
        ,ISNULL((
            SELECT SUM(RHC.NUM_DURACAO_HORAS)
            FROM CL_REGISTROS_HORARIO_CHAMADO RHC
            WHERE RHC.idCL_TIPO_REGISTRO_HORARIO = 2
            AND RHC.idCL_ATENDIMENTO_ASSIS_TEC = AAT.idCL_ATENDIMENTO_ASSIS_TEC) ,0) as TotalHorasServico
        ,ISNULL(AAT.NUM_TOTAL_KM_ATENDIMENTO ,0) as TotalKmAtendimento
        ,ISNULL(CASE AAT.BIT_POSSUI_PENDENCIAS WHEN 1 THEN 'Sim' ELSE 'Não' END,'') as PossuiPendencias
        ,ISNULL(CASE AAT.BIT_ATENDIMENTO_SATISFATORIO WHEN 1 THEN 'Sim' ELSE 'Não' END ,'') as AtendimentoSatisfatorio
        ,ISNULL(AAT.DES_COMENTARIOS_CLIENTE ,'') as ComentariosClienteAtendimento
        ,ISNULL(AAT.DES_RESPONSAVEL_CLIENTE ,'') as ResponsavelAssinatura
        ,ISNULL(AAT.DES_RG_MATRICULA_RESP_FINALIZACAO_CHAMADO ,'') as DocResponsavelAssinatura
    FROM CL_ATENDIMENTOS_ASSIS_TEC AAT
        ,CL_TECNICOS_CHAMADO TC
        ,CL_CHAMADOS_ASSIS_TEC CAT
        ,CL_ORDENS_SERVICO_TDS OST
        ,CL_CLIENTES_TDS CT
        ,CL_CONTATOS_CLIENTES_TDS CCT
        ,CL_EQUIPAMENTOS_CHAMADO EC
        ,CL_ITENS_TDS IOT
    WHERE TC.idCL_TECNICO_CHAMADO = AAT.idCL_TECNICO_ATENDIMENTO
    AND CAT.idCL_CHAMADO_ASSIS_TEC = AAT.idCL_CHAMADO_ASSIS_TEC
    AND OST.idCL_ORDEM_SERVICO_TDS = CAT.idCL_OS_PRINCIPAL_TDS
    AND CT.idCL_CLIENTE_TDS = CAT.idCL_CLIENTE
    AND CCT.idCL_CONTATO_CLIENTE_TDS = CAT.idCL_CONTATO
    AND EC.idCL_EQUIPAMENTOS_CHAMADO = CAT.idCL_EQUIPAMENTO_CHAMADO
    AND IOT.idCL_ITEM_TDS = EC.idCL_ITEM_TDS
    AND AAT.idCL_ATENDIMENTO_ASSIS_TEC = ${registro}
`
        )    

    //CONSTANTES QUE MONTAM O CORPO DO RELATÓRIO            
    let l_wInicio
    let l_wCabecalho
    let l_wDados_Atendimento
    let l_wCliente
    let l_wEquipamento
    let l_wServicos
    let l_wPecas
    let l_wEncerramento
    let l_wCustos
    let l_wAvaliacaoCliente
    let l_wFim
    
    if(c_query !== null && c_query.entity !== null && c_query.entity.length > 0){
        
        //CONSTANTES QUE RECEBEM VALORES DA QUERY
        const c_numeroAtendimento               = c_query.entity[0].NumeroAtendimento
        const c_dataInicio                      = c_query.entity[0].DataInicio
        const c_numeroOs                        = c_query.entity[0].NumeroOs
        const c_horaInicio                      = c_query.entity[0].HoraInicio
        const c_nomeTecnico                     = c_query.entity[0].NomeTecnico
        const c_nomeCliente                     = c_query.entity[0].NomeCliente
        const c_cidadeCliente                   = c_query.entity[0].CidadeCliente
        const c_estadoCliente                   = c_query.entity[0].EstadoCliente
        const c_responsavelCliente              = c_query.entity[0].ResponsavelCliente
        const c_cargoResponsavel                = c_query.entity[0].CargoResponsavel
        const c_telefoneResponsavel             = c_query.entity[0].TelefoneResponsavel
        const c_emailResponsavel                = c_query.entity[0].EmailResponsavel
        const c_tipoEquipamento                 = c_query.entity[0].TipoEquipamento
        const c_pvProposta                      = c_query.entity[0].PVProposta
        const c_modeloEquipamento               = c_query.entity[0].ModeloEquipamento
        const c_nroSerieEquipamento             = c_query.entity[0].NroSerieEquipamento
        const c_descricaoChamado                = c_query.entity[0].DescricaoChamado
        const c_sintomaAtendimento              = c_query.entity[0].SintomaAtendimento
        const c_causaAtendimento                = c_query.entity[0].CausaAtendimento
        const c_solucaoAtendimento                = c_query.entity[0].SolucaoAtendimento
        const c_comentariosTecnicoAtendimento   = c_query.entity[0].ComentariosTecnicoAtendimento
        const c_idSituacaoEquipamento1          = c_query.entity[0].idSituacaoEquipamento1
        const c_idSituacaoEquipamento2          = c_query.entity[0].idSituacaoEquipamento2
        const c_idSituacaoEquipamento3          = c_query.entity[0].idSituacaoEquipamento3
        let   l_totalHorasViagem                = c_query.entity[0].TotalHorasViagem
        let   l_totalHorasServico               = c_query.entity[0].TotalHorasServico
        let   l_totalKmAtendimento              = c_query.entity[0].TotalKmAtendimento
        const c_possuiPendencias                = c_query.entity[0].PossuiPendencias
        const c_atendimentoSatisfatorio         = c_query.entity[0].AtendimentoSatisfatorio
        const c_comentariosClienteAtendimento   = c_query.entity[0].ComentariosClienteAtendimento
        const c_responsavelAssinatura           = c_query.entity[0].ResponsavelAssinatura
        const c_docResponsavelAssinatura        = c_query.entity[0].DocResponsavelAssinatura
        
        //PEGA KM E REGISTRO DE TEMPO DO FORM
        let l_totalHorasServicoString
        let l_totalHorasViagemString
        if(dadosForm){
            l_totalKmAtendimento = GetValue(totalKmChamado)
            l_totalHorasServicoString = GetValue(totalRegistroTempoServicoString)
            l_totalHorasViagemString = GetValue(totalRegistroTempoDeslocString)
        }else{
            l_totalHorasServicoString = f_converteCentesimalHoraMinuto(l_totalHorasServico)
            l_totalHorasViagemString = f_converteCentesimalHoraMinuto(l_totalHorasViagem)
        }


        //CONVERT HORA CENTESIMAL EM HH:mm
        const c_totalHorasServicoExtraString = ''
        const c_totalHorasViagemExtraString = ''
        
        l_wInicio = `
            <html>
                <head>
                    <style>
                        body {
                            height:297mm;
                            width:210mm;
                            /* to centre page on screen*/
                            margin-left: auto;
                            margin-right: auto;
                        }
                    </style>
                </head>
                <body>
                    <div>
                        <table style="background-color:#fff; width: 800px; font-family:Arial, Helvetica, sans-serif; color: #292c2e; letter-spacing: -1px; border-collapse: collapse; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px;">
                            <tbody>`
        l_wCabecalho = `
            <tr>
                <td>
                    <hr>
                    <table>
                        <tbody>
                            <tr>
                                <td style="width: 200px; text-align: left;"><img style=" vertical-align: middle;"
                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALoAAAA4CAYAAABKduIiAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAASdEVYdFNvZnR3YXJlAEdyZWVuc2hvdF5VCAUAAEOpSURBVHhe7Z13nFbVue9NotJ7VcHeNTfJSaLGlHOSnJPPzb0n5ZwUlTK9zwCCigqCNKnS+zC0gRlggKENDNWCgEpHBBXBgl1jASlTee7v++x3DQMHcxLJH9fr3R+e2XuvvfZaz3rWbz1lrbVfLrBTlVZDVq2zTjo4QVVnUUj7e85erlU4cU9NPPPjVJkeHxOd8Guel8eIfNG7lecs9289exmUXyWqho9qTw8UHbQdOei5UySLmjx+EaWFW8qHx9Ce0/kgpVJXFe0TBfnWOmqy+l3tg3yBovfOnRd+9Pxc5O+e4zijoNN5zkjm8BueI4u/Ut7ffPD+F1AN39SFrESOl6jeGp7O4xDQT1d4qprKqqy6ikqogIqja6oMVf+9Z2f1lKCrxsA0oDgNQuWiYQBCOUk/oQcB6CfKTurvXy//vzt7/Q46lVqtGqpP8c/pFEz4hWTA81qdelKvnNAlvMAX3Ek6fk1LyBmItJCn2tvKW7QpGsDOjZcdSAVL1tApzl6n0s/odEjvelnVVlZeGatbpeoPZ8ibQJ/VvAuRM3pWURm1J+pf6tW5gkF/Zns5yA9FB3kDxZK+xMGrcFOuC2oTp1ZeU6QukHvlcV2qreWf6wE5Kq2iosLfq1DG86jeDwE9uqiqCsKGYCewFBHZyPFlyKuIdRyYqoSU5PiibBc4mU7XXCYEllVxFR08PlfZfwt50fwJVPuoSYvaXlkuHqsFV+SvFCAGlQk4lXoO0MsFPsh5F5VVHlP66XuGALkDVSvtdMugIGeIo9rrjMCpM4PSCblEcgOQFeXRoA98Cap2UrxTYtSs2LsO9NPlexMpngsfzORhAIoqTmosMZg8a5Q3uoyO/5Lw9x+hCPgM8nT+KyL+orYCcll2H9hqa1m54+OMPjyP4wL+UEgEOijS5Gil6pgmOiW20DqnJKkvS0HoVBLVExGNQAAAyxvGjR+xxtNpJJ5n/ZQdSLcxOp3oQIsdSqk5V6jqk2WRZgG2Va4BxRvaGpfL4RYIeVVKW1XZsapTdlRFkgN4opV88Igo4WyixLM71TW9k3JUofFUR+VJq6yIXLwyleWA0WPKr1SdUTtEMc1dVVluZeIf5ULZaEmXa8VRvSDu9Nxc0+uhDk61efCLmpsve4gXDTAsCmUjpc/FBu0upxHwCtjLaF/ZGQP6uBTPGfx8yeOC0A4IYQXhQVQHU1BIc+a+xJmyIK4Dca/uMxkrP1MPjSo/qSsf2WVyq9V48eXpor+1vtpnyqV86Fxt4jrwwvlzVeb11dhMd36iS65d86pkacZKuQCVAgsAjsHfwX1E9Ino4xgJVp7OGeL5Z6JPY8Q9FPJByAV+aEMEFmRRYeXHyaV7HWhFnof2Qmj5Y0qonX5MaZSF++DvunxVngbNFwHdk2ouzueI6jsl60HZn+s2tBE39aSqx02kmsBv6BPOpJ0vCzUaHZ8JAVH4X8TNRzp/UIu4p8NC5/09Z959X0Q5IS2kv1eLSANs8FNVqU6Q5mGwHxc/dO1/V88XnWvX82HsnmcQ16S9E6Pw/BMJnk7Bpz9VfkIhhCQTk7bzJ3JQiegwQMt7b+rBAT14UQ/2qDHbxfhWPdwmRraqwmf/YrZJgnhKzGx422zdYbO1b5o9+W50/ZSY2CnEv66ylOQ8I7fPVCn1lVfIqqCFcT9icQ1GiasPlRTaW7udQRbwCK/wHMURlAFJyPpXu32BTl+cx4FFwhrJFaMNDOiz+0NicR5pc+Cfe5QAfe99cR6HgM54if4igA9U4oh5T1rCoEL7jz5z7Xf9FtqvHtZ1/wX2h37zvhTx7u8fW2z/MWiJ3TOg2DoPWGSdBs23DoMX2B+HLrE/DF1q9wxdZZ0GLLD7hs22jyUNBAJPH6pnsvpOsg59Z52z7L+FqP+3/Rc5hXb8sV+BU3j2v/svsd8NWmq/fXS+dRwwx7Ify7P3VXclEma0EcOow/EMjumSzqIz1r9aYQPm77A/9SuyX/Wcaz/ImGI3xY2x6zqMshs6jdH1OLs5frzdlDjObkgc7XRt4hi7JmG0Xa18V3Ue7cT1DSnj7dvpk+2W5HF2U/xI+2HWJPvzsFXWs3C7PfFWVB/1IhtMfPlRwQP3JJY+pWSrZYwutoRRy63DiBXW8fES6zB8qSWNWmFpQxfb+OJtdlidjKXgHZcw7Qp+TQzQkfsaWTH/E0v/8gcDCfc34nP04uft7gHz7T8HFtv/6jXffveosNFvkf1pULHwtsB+80ih/bZ3vv2h9wzrPmG5A9/bfB6HgK6RJkYAFSOHEdV50EK7vuNIax83xa7ILLJL0oqsbXKhaO6XotYpc61ZaoG1Sim09on5dlXiTLsqKdfap+RZm7TZ1jqtwNomFti1CXn28+yJzgNa7B0xdVDsfb/DQLs6Yeo5y/5biPpbq27OlyTPsnZJ050uS55hbVPyrWXqPGuaUmQt1M6WCTPsmqTJ9k9xA03YOg0KddIJ2ViUARqyeM+n9sdBi+z6pAleVhuV0SxlvjVNXuDn1qnz7dLUQmuXPMcuS1IdohZJc61lcoHX1yptflRf+kInrpsmzrEmcbOsld65NG2uKN8uTZlt7VJmWMt7xtjvBfrS16q9/oinSjtx9IiDgEGQOHKpXXbPcLV5urXNmGdtshapLvEg/m7uNNJyxpS4ZUWzu0vADBQFQWdo9AB0Jf5DgB4VgTXZJ4H+MGWU93/zTtOtdfI8a5u+SLIRBlIlu4Q5dkmKsJBVaJd0kCK4Z6gV7PzELf35HBe4ryaYy4C5ADAnHYYss2sSc61VwlxrnrrMGiYvt8ZpS50api23BuklVt9ppRP3EM9qU4P0iMhbJ32F1UtbIRAstZYpi1XuQmuSttgaZBQrz1Kv51J1NBoRs02H0Hkvqxe/Le0HUM8uvzZFdUV8nOavNq20hqqnmeptDiipP7U4ejdtpdXLXGUXiYfGmcvtktQ5dmPHoc7Hx+ohJMSsFICSt2HjNrxut3eZam06jBagCqxBcpHVzSixizJLrU5Gqa4llwxkVuz1MIi4pq7AD7KIKLpuoHdoQ6OMZdYka4Wf6ycVSfZF1jxtkTVPL7LWSTPstntnWImsCEoJ5QRC4QvFED9mtQbvdGuetczbclGsDgYS/Rn/+MrIDRLRJscvfyjonED3m5r0Mw9eiPKc8Th2czqdfAK56mCAZk5cb1clTJLSUx9kqp3CRX3x2BDZpa2ybyUtt2Zd1lrdeCkmKYvLJd9fPrKgRqv7AK85Ih7OPKL7M+qXQoi5LtF0GtoKgMWNWG7tOk+Utl1gLTMEcAGzZYquRQ5OdQid6Z2aXuogqpe8xFpkr/C8jVPVMVnL7UIxS6fVT1tmDdSohlnqZAG9kfI3zV5r9VPVyXqvVXaJtUiWBpRGvy0n116N8UHj9moo3xr/uLWWJWiu9xulLrH6CRr5Am2L9CXOTxOdAXEAO0CCvxoSj3Uy1vhzNFyr1Hy9t1B8LrcmKStFekc8N8paYo0FWKzX9xJG2esa/YACm0cAikbKf/mUtUmYLG0rzZs+x+olzrYmOctUjwZc1zV2YUqxXYxC0DVpdQXUZpkoCQb0cpcZvERKIuIVaig+L05YZK27r3WZMPAAP0BnsDQW6NtI82GR7uwxx94QL7gB9CY+Ov4sLssl0v4NJJN6mbHBpna1SpsnqzLLOo5Z72CjxwGMA8Gnobg486h5HmXSKabhOTwN1ahgXGmUR1I1LhBuXiytIjbzw0MG1jNvVtvN6ZOsXdb8qA/VvhZSNs1SFwvoq+yCFPVftw2S3wr1zwprmqb+EP+XpUyzKU+/eeYAdV4oO5pZIs3TY/UxKODBA20F8WfMuhCZA7CO8puvSJzmwG6QWOTUTFoJAsgNxMBpjQSQl1hTDQgfBEkaENnSXJxzIgAB9ibZq6xO8iJrlqMOENi/EVdsjbM2WNPMNVYvPnIb2qfOtO9lTbbXxAMd9774IbD7bvIoAX2uN961AIJJWmiNKE/XzQUi13qipgJD0/RiBz9aEWqYoUEmoZKGq9QmbabnQ5s0TV0tDb/SmmfqXu4VZbaTe/GD+OF2WH35F8nThSte4Oln/UqsTVdp36R5NQP64qTF3jloUQYcoCatXqoGo7Rr4yS5NMhO+ZEdxDXEwIWQHW2p1ynfZYlmY5BSfhO5Q83R9IBC5bSPn2pzFLHS8ZWV3p1+jesC0OmPugIO7yOrFnKTWqXOtbvGPhkNjhhg6XMHeu17/xMBPdKeEaBYGzjTlQHoZUqNLIqn8YKCmGoFyXDF2gPAP36iMqbN11rbzhPcOtEnLv+EAh/AjbNX2zdl3b6RKMsn3pFJQ8kQ7LSRhf2XB2b64EbZRC2GF9XMVKnzFmMr1pYa7e9AL4uA7kzpOeYQ1+WuoStcu7aWcJtTqbQQGgZyBpUOmGAYgLp/iW8qzdMqJwJi3YT56vRi1/JoNYTPPf5rq1S9k1lijd0alLrZqq9OvCRzvv3w3jw7JB7oOGZ/XhXHt8SNcP+tTpIsA+ZdZaK5sQwNJBjcoFbJs6WJ8+zSJEg+atIcayPC322RAhW6f0yeNjLvTaXZHWTJq6xxMpaiQPxrEOh8RedJ9qP4Qfau6g/BH0Df9NJxa3PXRGupwdkwXp0gMDbPWCfeV8vcbrKL4pZb+5xSxTTiKWGhtc9eaa3jZILj5XPL74z87ZnWXrEB1E4846tD7dLnu7aGLssS6Luus28la6CorWg/3B40fRvdXxKfaw/OfrYG6Mydc41Gb5syy93EC7PW27cyN+i6xAc4lux8gM5CWc1z19oR+MMimh+8oGcAPVo0i5LAVfFLJz0Iv0QxUDMpE7dWwlMTuTCNZUFxXxpnr3FF0yZniTWTN0E6blebxFl2ZadRNnzlS95OPI/oUO2xmScI1k7zrcN5jagG6DykAALA1McXe+R/XcZMuy57nl2VPd+uzCkSzVeQUGA3ZM22mzNn2C2ZuXZrRkTf6TLLbs6YZtenT7erpDG/8/AquySjyLURmr1eykJrLU3YuPNMd4naSrNf1GmhXZyMf6oGqzPapc+2f0ob6YONkQsdUDtuiR9ll6Zr8ChWqIsLkCmAZpU60JslL7afDnnO/nPcRnVkqXUcXWJxI0ut88gN1nHkE3bP6HX257Hr7K4xGyIau8bv/3PcU/b7sc/a78fsFu203+jZnyZssM7jn7bEx1dZt5EL7H3J4ogEc0xyRDYjF+6yK9Qx9QTwhonyJTNWW4MUDZTM9XLTllsbuQvt42bY5feMt1sF6N889oR1HvecpU3bYSl5Oyw97znrkvu0dcvd4JQ97Wmlb7PEGTssNXeLJYx/wn7dt8iukA+LdWqcJZcwUe6iZFM3RVZUQEert43L9Rmj92p6NPJ//yvQ1/2DgA6YY/fujkT5o+0Qsc0QXoz++Ps8jxZ6PtMf+vPfh632wL+1LFdzgbxuqryBdCnS7MVSkgVWv8Nsu/b+dVJGcncVtDdJnycrt0BKVK6XnoO7H2RNshc1aghMWeKIkCs3iaBaZ5JQSqS6C+OrxBFLNa5LVZlel6khEzMN+MfMfhB8cQ7zmoF4Hoh7BM1o48w7+0UTdpvPemCWG4sa4ecz45CxyOpLE+OjN5fmwjVqLR/yO5mTreTlo15GmXhnrO4Twq7rLNclWUFdOn7rQglonluHRvEL7GoFs0OfLnMrgEB5Fw0C0ancB/6+iEKbuA6LN++pXgQKwBlwtDN14hq5IfnuFtBJTbqowzTw6sqiNO8iTSvX646s0bZk78fOS5Ab7iBBIHVQHt0C0SlBZtQBz9RTqigYjY/L01IWkrYSJOO24bdeLvfr510nepmUc7KyystIGLXUZ5IiH32V+/64R241zst1+a9Ad8xw64gBUKTrH8Ee78ilwLmhzbN2lykGy7eGOavtwmRZYQ1CgvCLcT/lMrZJL7Q/j99qPVd8KIs21xolqn+lzOrLhcOSoeUbyipenT7DehU+72XigURWJfAR0WmXJVqH4Zo0B7ozxuyLHlaecIPo7BOEATYY9nHCAoPyhsLJA3HNtgUKBBwsvDxcsN2u7DxegYdArUCqqQLOb3ae664Nbg/aiY5r0HGWwDrHfjN4hT2vqqk9akS11/2cev7GpAnWQq5QUw/aFsjMFcilWmrN4uba1QLenL0RyAGKNzIwSLP8MuL5NFFB1JaQWCWhVJ466S4KSbz+eXm5L+cz8Cn/V33nyMWZaxdL09YnDslcbBfKDWugNtZNmm1XZkyxGZtedTAhB/iHkCPkcmT/SqVKrBS3VccdZ8jQVzsFlpOqmMWTnNk77Mr0mdYqIVcDSG6Zym8ZN93p6sRJdmfqEAc3C33IiwESVysYddeAWSa5irhjraVN/yFA/y/3cF9Lliov2jxW4YMav/pf+0VB8TfSBPSMDXZh+lr3z5tlyP1LmC3PYbptlEbYq9duf2ipuytMXjBrRDuIdepoULTNLLLvZU9zTFA2dbLtAT6oGoIbb0IM6NzTBxdEDOqWDTUsMbOLjNUyghxGp5fAtToHUgDADATQQUbeUSIqRsOgjX72kHzwznk+Y8OIxAdvkLPGGccvwx/3mRyZqO91mW0985/1OWuAinaKBFdhR1XtftG1CROslQZLo5RlAvk8CanQ2mTJp08qtKs6TbG8Z4963YDLm4rJ0ilqdQS3aNYgHHQWD3Xm0tsY1cl1deDhlEo8xYatSDP/MHustetaZBenEDwtlVaS60WQ3rXEmsqvvCJ1ir2u96nx83INGhV+/OSxqG4fQSrJNzAJ7G5Wq2It5Yj6gWlM2rFo10fWYUixdRyxzOJHr7SUsaut49BlvoDUadhS6z56nlsfuggFQ/s7ji51zc0EAVYHje5z+lIM5wt0kmOP/OBZdB8TdMggqqqINC11Tdn0kbWLn+QBZ51k9Xu39QrcI+C3yyqyyxVY50zd6AqO/ANWvWbt0+cKN9L4qYrfFBM2kZJsznuJskzxeda9YIf9RRVQB25LrNoacp7pT+SpO9fo0RMeiFnf2aZuCttVAbubJM4aBJB3i+71DoVwhdlF44174i27KWOGfPOF1iy7VGaKEbna50cvSJGf3mWdRmextZYmhOE77y+0op2fuWZioETirNBYYkBFizNb1ZvXJoxTR8kypDBomKYrcrPOYsPVcnkmbz/lc961XRd4Cu4AAgzuCdekIdggXCjcQ2jwSGBqb8URv8ZK/bjrVF9Q8pkkdyeWyo9e4bNJ9TRw/8d9Re6mlAfwBAuIhnO5RTMUALk2bxBpQfuT/qbosAgFANE2tCPt5Bn1UAY1oRB4TjyCL45fXjdztQOd2Z7WchdZLDufYJQzj0J6lC08g3THrR6EqWpc2B93n2XtNcjaaMAxq8SaSYNMKSyB9tLE2fa91Im2XVoklEJbv9tVFiyrRJap1DV5o4wlHpgD9obJihOTJ9k2jWzkFfjgXMOT45XSTvPsrktt5iPNx1ZUgoloPhSmOZPHtZLuKirKvNMAxku6SJr4lF3aeYK1IQCV/8qsyMXJy6xplgI2XV+YrrPAz8xH+7gJ1mFEie1TT9HBLmsYjDXXd0rqio7cLdRdL6AzrXZxqvw7mTLMGqavhfzWpgnz7OYHVtntj66xH/ddbj/pu9R+1gda4fSTWBrPSP+33kX2q14L7F/6LLJ/7lNsv+i91H7Za6n94pEi+9dHFso9WWZ3D1zsAwZuONiT/o4YukOuSdv42QoIl8pXXuYWBs3TULED9INeq9yF8PYE4uAcm3IDoICVKVTiCgjgAgo6+Q3lxbcH1KwnkAbgec47EM8OiycGBL1xRAXzzn8H9M6TNzvQo01pMfb8D7KvfR+VG+WKAScmDfoc8qMWoGgz+TnzHKUyas2r3tfU3TI+39plF/u0LjEN083MRPXOl8+td0INWKnJG9/x1eTGki8KJcQcDeUVoFRaJ06z5NHLY64bDLPDNsa6W8rIMvqhRCzMBdyGSgA0+8B5DZCXKzUy/NJyyshzNvHzDmlUtPLASftJjxl2ZeoMX8JmDr1BsiLpjOU+NdkgKZonRgs2FUi+nZ1no9a+7q7AEeQLd5WUdtpSMNSoA826S39wXZooGK2XtcYadVlrdSSAixTMsrjDam3rrqs84A1Bb0R6LiKQg1iBZZEId4eAp3kqi18LfZW2lcpmFY5l6EsU5d/5YLFrcLiCPzoOoN2WMdWBzuruaaDLyvhUWand3meNdzBeircrkIOlwj5T8/746Gz7/r0z7fs9CuyH982127rl2R1dc+22nCkux5/2yLMf35vrK6/QT7tOsp/kjLN/zhlvv7xvmtKm2U+6TLL/uH+MHRAq4I1+QaN3GL3GQeULdBmSvYCOgviHAN0BHcnE5RJrUwhCyesY0gVgfUmPWPy7Imuuu08sCDaR7JmQaJgiiy+t3j5pmu2JtcG3GKsgynhPRf784UV2SfoCqxNXILcwWrlGqzfpUiplusCuSxprJS99HuNFwNY7kYKJgM5RpTKjLd4xoEMENDyOtVMXqraivOarFtI5wxQNQbBDS16yG9OmWuuE6Q7yttlLFFzku9YGOMxvE0Bisi5PmWm/fLjAnjp8yoMtyvFDTLA7MJTPOQCdTtkqtX5F0lRfMMEVYlqqkRodFo4wgSwXnw3siCLAYy7rZLI1YLmDGiJQi2YllktDyzKwiEPcIODe+dAK15CuLcQfvKBZf5ittspV8hkkmVM0DdfUw0C6s1+JD36rcpWhd6HYdcUJt1Dfz5zoS/n4zW2SZ6qzp9qVyVPsssQ8n0MnrWmc4puUOb6uwGIJe17aJs30Z80VlLaJm2Q3dhpkL6vYz9RvaHYUR9yoEt8C4AtRsnhRMPoPBLqeASzoXECnpVhoeOldtN8XtphOZktCQ/Vfsy6rrUnmSlda7OXJyd/pisEP/B62DCtWBIfTn37TLuk42tcNLmLlOqtUfR9tkWidJUWj2OxPw0vcozghnAZ+/aMZ/0JOh/gCWw70qJFAy9P9AODRHmWqjNKJ7o/r4iPRQdFdCozadhhlbdMKveKG8qGZUWEaCMEyGgk2mTNnU1HWtGfsdcpQWdHIE4NHNWRiX1rwF4rGNTal2jvlOb3QXu+z3F9HI5oOZCsAwGR5vKWCwrbdSs7Q6GeC/TTQWRJvnlLsFPam+IKVXCLm5Buq/MYJi+z73Yu8s+CBDxPoSzT87TmTBNI8twQR0Bf5wgZzvvU1qH/0aGROfYuxWyjI7aAoWtRBkzdOVWCt9xsJkG2Rk0BNcA3/pGH9mENnywQrrICEFcKLE4useddSa50+325KHms7Ze2oD5niMrEy2i55mgOdIA6NzuBlQY8Ndf8IoKOgIiUVSwegSmdem5bCzw6N6JvTZ9hlmVJ0UngogXpyVS5IYOV8icdW16fm2rMaoQx+/9CCCQCfADnhYMXF+1VvDfKsJZHLSlt8XUFlSnb0+6Wy9EXb3/H2wxP4gedoXp1DvMVcmWhTFzfeWJjWNVMzHniW+X4Fgh0KY/QtOVDlQmaFr7UawoIQIIfoiNY9NvisSvOkedYmPlfmuVCR9we+yogm8HnWmNCQ6cnKyFwFRj0Xsx0CB67LFrEB0NkEBNhboqUViGIlWmdJE6tels1Z5m4hwHFmsYVFkmj5n302EPfF1kJuFBvI2AtyseIHfFkC5rpyQ5pJ47Oqeed98x3o7ORwLSNGuf9pl4mueX1DmtoIMJmjZm2AAXb7IysccHQ67aG9SJZ2QZTxXQWsbAGg3ovl16PhIteOdYXTWxd8Wi2NQDfawwNwWTRq3nWNNZb7dFXHEb6vHaDQU9TLghEaPSp/rV2UsU51rPZ34fG8gB4D9Ol0HeTlVZ25/FgPsIQ9Cl5w/7tBIlskFHiqz9j/0zhnlQeZ7RNnWJfpmz32wBp5v6P93P+odu8CrC3Yf9Kad5oWTStLOflKtvod+Vwk95j591/3meszTrTrhLAEb86WyonaCYPVAjp7BdzE8plYlOjdI7CzOZ/OQhMRJD26cKdd03mkbxv1RR91TKRxVlhTdpypIy4S2FtmLtJom2a/e2yFPaPeZZS7FkB7449rYCG3AIDjuuZ5JEAGGRAv807cKsxfmTjZo/bm0k4sC7NszNRlGwWjDTvk2w3dl9u1WTPshsypdnPGZF+hvSk9367PmGvXZOWLZtg12dPt+szpnn5DBmn5dnW2zplz7VrRTdkFPmN0S9IU+32vWb7PxjtAMikXYwjzZ10myzrN0iCTm+QuT7FbL7Ql8/o/fGiJdxDtoD1ouNBGfFeefTcr17frspaAtiVWwM1jrpuVw1aJM931w1o10IDE3DOPjKx9n4xA31pWAB91mzoGoNQGOtYTQAB05qwjoK9UPecDdEiXjg+sbTR96H9IojN1AS9bZKSvT51mrZiQSIu2djSSxa+je3DSOHGeXZ48w1Yq6AHoBNYoAPjCJQZr3OMqviC6ve96V3AoNhaOWDhDBuwraiFtf0mnsTb5mXdrMIa76ayKKk5/lwnQUe1R1Fpb+5xQpMqLVPy8pPjrgSusddwU30dyedZ81yoEFET4F0uoFyTLnei6Wq7KfDV0ig0s3uOjm41ilM0fd4k8OIiEyNbNIFDqJACOuIjMPX/3CPM3xI+RGyRzn8bKqFyUnCVuwhonFNlViuJHPFtt+5SXGQlcjA9VzIcq8H0V/J7qIpAMxLI56Z5Hzz7S+SPdvyO0ICxmAD5UxdR9QhzBFwMOwd9x72yBskidt8rqpq/xGRdAyupvK1mL73YrcNfO26oTCsqR4DKW66LTPX2m248UfFLWHT3yFYhOtzu7TFGwOdF+lj3OfpytoLN3kV2XU2R1O862Ftky2wI4K6RYDaYxcV0AOgtsgKO260KAx0wL4PYt1AIEm97Oz3WhDbp0oDOsohkkf06SGlpZcdzLTp283vezNFQ8VS9jvc+u1JOFZf3kW/FF1rbLUslslv20X6n9y6MrfYHoJ72X2S/6LbOf911s/9xnqf3oIZ37lthP+q+36x4QruTq+H4qKQfiKjbQtbh3g++hYmfmHT0LTHGpK+VyuUFVldEne1gGzhgLuS5wGk0P8ckamREcTCO8uZvfttsyJ/lmeEYUc+DNpXEw383kKyFQTCzmhMWKmzOmWuGeY/4un+Z5+bhDqiNaNIGZSmeCgcQsDl45DEHuF/tnXidcmPulJm6KH2UtBOyGmWt8yZ3ttJiwtrrmI47crWUOcAYlNUVmUGcvUNZDNTFZyjMWt7zJXodaWi1I+9f21KtLMug4ruAR/pAHwTNTgrd3n6M2yt9MlfVKX+e+PRvU0MD43ewNepNKKAIesF6YfdYlNMBpD2WhAOAX4hoNhrYPmuyAqM/az+wy/H/Vh1tIZzdOLPBNXs0U8N+cMsF9YfqJgYjFQaMT3DYTqD2GUWyCdme/CFsDEic95YOZJkay5kAYULiPjjOeu8B0eAJARwVEefxVpSOrdS+/bzekjHVl1zhVcVPWOrswQf0mTc6g87iD2S5hiWnoFnJv2yr2aKrB2yipwFplRW5p2xwFnfF8mBLFQGHnJzN5+Orur6vvmb3x3Y0K0kevPeiD3lnES9HBNhLnUzxGC0Yi/Hd8S4SGwPfpPiNvu12bmOtzmmgFTEbwF30PgsDGrkVMLsHFb/rO8+8iESbTkX4gJBXOLR0dDq79PlY/1wjLX0MDVohtvbtXPclnZq0VALqWUsej2Vgmbi8f7fIOY2zO7pPe0fDu77tPqTJck9LoyGLxLFadDzr/dQDYi/UjRwA6zwLPtAcz+7OHo+nHximl1jJrgwIrBYnipQlunFyp5vLf12hE4IoZ35hWiiMVF37PhPZhKQIhb7ZOMOVbpgykUReD6r75BxQPMMuy0E13i8TZvpLILBMD65bU8barFtAZMJ1HrXKN3kJA8k1h6jOUENdXJ4y3tLEl3rc4hnyFH7UUZiMBcF+b/HCQn56sOOXf8sIpLYrkTLso99/7zrZrcuY4OJslFrq7QZ/xPQCzZcRBKETAybQw09BM0YYZotqEa+gf+ugaYNfP3CCLUGKtcqRg4hT/yZVtRdCuWAcX8J9kTXdJKQJuX9n3ODBqF9OMNUCHYB2t+IR69Y5759jlAnBrAblpkoI7+YmA24MkmSH2OnOPr351VoFl5L9gL+td3AMWVyiHbbZ0Ap1BB0LcQzyHsB4QGg1iVMJHFH2bbVema5OnuEZrlCktoeCNDmTjD1N9V8mnHv/cMQcHYKdMyg/lco3vGOqDF4i0cH028c7H6kuGCMR7AClu3Bpr0VlmOVEuWydkovbnKLCV+1JXPjqa6tf9S+xNZCnCgOHKAAIsHO2jLOqGN8oGJGEA4Dah5Xco4TvZs32+v6VAgZvWVoE/lhRzzZ6PW6Q5D0i+lPOpzvDHFgBmXQA6ph45sZORRZcrk/IsceIm7yMGLbKCH/qENnPG2sAnxDVp9AdUm3/y00doSs7km7X9U7s5Z5bzyFx5W2n1VsyZy72rl7FW2rzUV8sBdZ3Exf5NQtOsKBhn35OvR3wBAfSLkxRndHnC6ics9m2+l0rRNeo8V4qP2ZhS9zgeLHrJeQPLPq2oQahLV1ix6UXY9TkGe/6No/aDhIF2S5fZ1iR+hgdLzGwE8xFmBPDNMUd8SHFZt5V215w3rcuSDyxpxgv2wMID1rPoFXugaL/1Kj5k9y/YJyb2Wa+iF6zPgp1O3EOPLNjt9w8X7bVeC/fagPnbbcvrx93NoBOfF79oKRYdWAhBQ0BYFjoSwf775D2WNu+Q5RTut3sL9lj3ubt03mXdCvYq7UUn0nsU7rT7C3ZYT50fLtxhvebtFO22hxfssQdEPcUfPPZftMsem7/F1u//uKZjOT8ya6NdJTPZWuCLvnaSRmG/e4Z4S48Wydh/fuf9K+yB2S/Y6PXvWuGbZrlC17xXzRYJmYtfrbIlh07Z8kNmK147Zater7ZSRfpLX622mbuO2wPFr9itXfPtskz5pdnL7RuyGhdlrfX9IXXkhjTIlgZLnGPfSRljhzQgGCQMGNY1ADqzLrgufCuAtcG/xwI7yASQGx9cY9fmzLObNZB+0H2Wf5r34wfm2I8eLLQ7Hiyy2x5aZLc9uFjXi5RWZHf2nG8/Ed3WY779qOdi+17XAvtlzzmWu3K3D1iUAoPmN4PXWUu5Vg0T5JokFQuIiivYYpwixaR4pm68sKP6IdwxVrXRxOxcxc3yWawvIJ9hUwDOFCzuz0VJUczCl1tsKWGltWFyod0ouW2WVnGwx3CNAkE+sU/pQP8xT1j1wnv27cSh1rTjBJ+rZFGEAgE8YPM5WpkTTCJBAUFG8y6r7JIu8sPumeYfO/CBAbvs2MPePGGWf4PJbAALP8ygcGZxBJcHEHPPttTLE6f41+8sFgSNu1WcXtJpvPt0BGL1pEUZaKyQsie9rgSHacact+HjC19YmR374KJQ/EqQ0rjc8wxfFV6uTM6tIT7SZtcfuwQJtuHxn5Ift5FLt3ugirAIqouff9uu1rMG8fO8/bgrzTLpzLm+7wVN1SJjtTXprIBcsmOGpkl8njQwH2RPVDwxWpp1nF2RPE4DZrxdLT870OUJ46xt3Fi7tpv864TpVkd+eAN15oWZpfbN7PX2DfzdrPV2YcYqa5qYbz/rNtk1LNqKHgwro7TD28ysjgOdxSwpppwn7Jtp0f505vCJtZjDb5aIFs73weHz+8xuxIi+DquafG3GhziNU5Z4sPlI/hbX8sx3z37+Y/+IBHcDDY5/zhRuY8mjqdyWZtLYV2vgXp8+0z/RvKzTBLsubYZdLevDPR9KX5kINs5N7Na8svPjwslktxYNFKCHWI21DCYoGmbJoiZOt+z8591ioQCQDJim/wR0dGcUYJCweNf7dlXcCGspLcX+4W9gZoj6ZSoADvPmCAGwo2Fdy7Ioo87HXLbvutTnbH1gaKShdRFY+MInAJAy0DZYCoRJfoB/eafRNm3LBw5yTCYB1w2JCnBSFfDROKaXMhlky32lrGlOqQdrgJ36Ia6Zj0aTMV/NTj73DZUGAHjO/LevGDqv87wM8tAu/GGmUYcv3uYywfwBptd0c2tGnlu4+llP2LfS11uzbmusjgKoOkkCQve10qJ8jCyNnzDfF8uYIWkmK8BXTW00oFqlTBdoZloLDapmUgCBWmYUWiuBgf3uLXNkKSTv5t1K7MKkIrtI5v2C7A12YZenrI5Ae0X2PEsYsdiVAUcAOntdmBDwr7mwvN5HAoPkdaHA1zBb/q1kwoKLB3nqD4ggEPfGF8GUFyLfaZlJXlI0BIxo0Fbif+iSnQ509un8vOcMqx+fr8G01vmDGJD1pIiYJWEj3MMFW23Frvdsmxjd9EaZPXXwWA1xv/G1E3+VNh2ucPeofdp0l81FyQulaGQZcmjjAquLiyeFdm3WdNsgrc6cPtOgyAbQxzQ6PxRU5r7e4hc+9elBvomE2WhBRS6KhHDafYk+IvagVMIEtHwbWZfvHX0qSJoAoYmZFt3WigEFFwIppjSAn4+EL/KPhKOlecpBy7AJKO/5aEcjGosA49vJY41tpgyoAPQL5eMBdoBGhwQN5PzpmgGI9mIzGcTqGhaINLQxmg7XgzaR/0IFRXWkNZkqZTBclTDRhhfvdI3gQa1MIe7LwOUH7cp0vhctsW8myoWSjOCJlVGobgJfxkTffzaIm2ttspe5G9GSFVBRi4yF1lTtxAI0zFK75QJx5iMEPjZgO8FFCYXutnxL73OP5eB7SgYsu/faxY2z+VvfsaOo86po+TsC+oYY0FkgY2VUfMVAj7uHL1tf8Rbf9QJ01jvgB02ObFk5ZsWY+Kt2v9AeBmCLDA2OrqtkBWbYQ3Oe8TqHl75s18oi8f5FChRZGOKj8G9JLvXlZtVLKLDvKd5DSQA46Kj+MCMX7sNs318j3FhcpIeKD/gWCgbrt+QaNSIWAfi4dcJdy4Q8S57ylGMHZEc/IciCEQcqSwf+1oJdn9s1CZOspQTE8niTrDV2YXyxC4rGIwT/ekVCYwDUFgbTQpd2kZDj8l3zA3Zci6ApXbuLeAeQX8inXqFcmUSsRtvOk2zSs595AAfYdwhdN8YN970hdSVEwF1fQYy/pzOdw2d6YRAFS+KdE6uL8gPv0Vf4Ee81ncjA0/M6OevdvPNOu8RcG7B4twuYxbTqqpMucKb/7h6ywq5JzZfWljVSXhYxcBOaMGUmS9hWGhlN2VIaBx/UV/SSJcNkaZ8ULJEsUqoGelqp2oP/vdbqZ66RBt/gMxMEasH6IMMWAjtal690LpUp/8OQJS4fAM70KUEhIOggjY5VZBAHjc61W7eUYo8hUEjIB3eOhTe2MzBNF60gSysSHKp/XT4xrQ4fzMPjPqJImATorXhqn+q9vdsMuat5PpPCO1g7yieIZKaOj+wfW7bfrQ8zrhBwg3wWWGWE+79Kyod13aYo+dupk9xaomzZKRtwhdK6VDxe0XGkrT9wzPNX8kvCAnvNp3RMbwH0eVs/lI8+3trGy7wyIS+t3EYNhekACgTHtYOZaR6WwTXi2ajDkm17mWDfAyOXgGeAD3cmEPdogAA0F7wsAQtRTINN3vSRawuAvltA/07CUDdZfNDMyiimFC3kARf7aaRpcG2IBYgPIK79yxr8c2YA4IP88B7jHz5wpSDq5+t7Pg6An6uTp9rA4t0uE5//l1YAWAD/kCTYa+52+3ZKrrXuNCWqh01a6XOlUXKtOQtrckWaM2uiMn2/CfPZLN6ImsqiNElTnJEmawDF9tmwZM6c8qWKd5pJE/JbNv4jTAnT7PL4iXZj0hifwt0rT5OfqPPjVJkDBg2WMLJEPu1ktYeFLWQtd5F9NKy6sqiVMNvaiU9iFWTE96Vt1WfkIS8uHDIJ/QW42Qx2iQZYe7lfbeOnq+wCu0btzJn+nA1Y8opdnzTFfXbmxPne83KVfaU0LjNGl0u7/jRnih2ShkBJ1D5qttH+rYfwyhu4tA9Me0oDaLo1l2UCoyzitZeL2IbvdTXQb5RP37HvDB9cLqbKijO36dKJJdvftR90eNRu6TzKruk01m6UgK+VhicII2gMhEAhAgXAyVZaPpCArklUgEUAUeudK5MoY0LNM9J8516sjGsVrPFB9q2dBlne0685yHEVdn5wyn6U8KjdlDDMrlYg5+RlRER51yRG9ULXJ4yJXSs9xreX73wpEPR6owCYdPLfED/Kf3ELt4mguE2HsfY/kkfbqGW77BMJCGEhJ7QPc7LICiFufrvcHircand0n2E3JEf1XKUyrpTMCHL5trN1mkDETkQF5e0ENH6prL2AeLmCds4ErMyXAxD2gACSq6Qhr4yT7BSEXyv+v5c+0ZKGL7aCZ9+ww2IGubDgFn2pRHxVbZ8ovfuYRXbj3f3s1tRxvqB0VdxIuzF1gq51nzTWbq2hMU6ksW+Jn8K7PmViDXEf5R8jpRfRrUq/Wi7TTemzvK1ZUzfbL7pNtVskV9p8TcJku1bx1Xdkfb8fN9RuvGeQ/SxrrE1evsP5RWbI8UuT/rAVA1fnVQVvd2RMUlCr/tVg5ceZ4OO6e4aJz8n+41O33d3b1u581/NjDS6AgeArsXmLDnxBofRzso1bRLgOm3X/jGwj3/U9owhkk8zH5hg9q/uziXTybBQ9LVUDbRRtEoX3eB7K4Z2N70Z17lA978glZlGDLoSfnQIU2xD4gc7wfqCz64b8mc41deia9yHuA09ck7Yt9oz3tqjCTbE2vyZ1jlx8MU0UbelQSjUqIdqioNd8Dn+zylv5hln+jpM2ev07NmDFQesljcd04QPFr1pv0aPFB63/0oP2mPz8oSsO2bCVB2to4JL91n/xi34eUfqaTdn4vi148aTL/BX1i4p36wKhHaNVZgmKn5PmdxiV9rYYOqiHB8TrC0reLXpRDzhjBV7kPnYNvaD8u89BpL+o5/uV9yURZ340dZfOz4u26hltpqznpJG2qbO26LxFfbddpvhFXR8gj9hDYelVVxQAluPLnMEpXgd9QHm76D/10zOiJ1XvcyI+5Nmkfnv6HbVPqn/Pu+U1dV+A0CAKghAYDzERgAzBch/yBSLf2VT7Oe9AksUZdHZZZ5cBD2hONcmpQgjzRorOxUeopzadXSf5aCzE8wAYrikbQbKixpdE/DwDg4z3CEF9+d63LOsa8hVXSabyUw/g4Zm8yItOBZAQ1xAzR5w1hpy4D3KtzSP3pENB9uRFG/I8yCWw4fKpVAtinz3WlhM/y1y7XNoT2nx2PbWJNJ6Hd5HP2bImHzx9LDFwDwnTNe+Ge4gyQl3I3nl25v++M5esLkdlVEjxlHk9zoeI8uGPvsClC+2Bn09ORnJxoJOhir0fKoQzS0fcOQjYAOTLqWcRne+dLgrfmNY8E4OBPC3GuBPXIZ28ep9NZbEN8pUno7NrK/aK+CYweNC7HGeXgzCCYL6I4IMfuvGdmuSPHf6cdNXpW5N5rqyx4vyPr7BVRl8Q8ip8nZII+U8AYvkpvlwUOh5CflEZUWFqhXcGZ0/nCpmLJwYM90QBVEMeKLqO7carYeD04VsYKtWR5fBEArzw4QF5Y3xVRO+HQBBi1RbSW9E151geNxT8qf0CJo1lfwXkgb/IouiK/mPA6eCZUmNH9H1sGDi8E/Xj30/URVn+E3dYMA0j/pMKnEjfG0VL+FUFH14a9Eo9qk6EK+pVKbguiJdbvXiKL32iuUdEjx/If7tSXXHchYd/6tj1RirDF5IyecaYIDjHHtUcIZ8zWu0dxoFcuYKOi4FwTQlQuK+p6hzkR00CfMR48fpiyTHy+vWM37Xx3x3XQfmAINTjwPF3SZUYAWeF5ALPvK5M8IbMEDXruhV8ZQTxP0woG4BC8KHc6I2oPK5JI4U8UBgUpAfyOlQf+1SYniMPHPt7SkN2fujsfRRdelfUNLg2cfg5ksE5yftRFCuDYsuomHs26zCrEZND0EmhfPgJ/wcUSV/24N3Q/krwhHz51YoIpXpYy2bxkyXK6XlF5cIuVxdEoRUdh7KPTPH7xyvdHJAl8E166ID/joLww7ucSa/dieQJFN5jXpg6YTmYHu6PqJCQzhmqzUso91zl1zRY9EV5zy6H8hFdcGH8uToMEYd8nEP5bMwK7wUeeebt571Yu4JMyRverc1XeD+U4WlqO78UhgxCOmfK+ljp/OwDsiI98ASfbIF2vkWUAS+BgkwCOZ9n0dl5w8ckJ1Un1gvAe+yiCnygx56Tlzyf64LNbYGH8yXiR+QBoUyoi/8tg3a7DMXEycpogxnkstZz50hW+IJo5KqoWPS+bvMue2jkLOs2vMByBs+0hx6baH2HT7Gew3Ktx9DpTvcOnmbdHsu17EG5ljkw1zIGTLXMx6Zb5qCZEek6a9A0f9514CSnLgOn+H1EU/w+Souo65BpltF/sv8Af2r/qZb46FTLGJRnmY9OsC6DeD96lzPvhXJrl50lngLV1MO7Ik+P8VSbSINoR1q/yd6WbLWPZ/CTOWCKZQ2cal0Hz3J+kgZMtpSBEy1r8DjJYJx1H6BzP/HYX+WJ78x+40SjLGvAKMsZONbbkz14diQf1UFZ2f0nWU7/Cda1/zg/Z/dXHQPU1oEzLH3AdEtXWRn9pliOqMfAydZD7UDm4b0eQ9QfI2fafY/Psu6Pz7b7Rs3x/ug+eKo9OHya+m+G9Rg2zXIem2rdhubZA6PyrafO9OH9w/Kc7hseUXSfq2f08RS/hu4bNsO6D59l9w6f7ZTz2DS7f7jwMCLPHhk53XoNn2oPqY57h86yHo8XKM8cz9d1SNTODMmri8roMWiMZT8y2LoMGK+2T/5SlCN539t/rHUfpDL6TbSUvhOtq8rOGjzDkvpPt+QBM6zbMPExQjJRm5BFj8fn2MBJ8yJDXFkG0HXl7oXArhGAVsXJV7DvMwqMDD7ADY5/beI5+RTwOjH3Ha7D+2fnD3R2Gu9wpoxQZigjBHbkD9fwA4X0UA7vhPeg8OzsMgOF/OzZCM/IF/gjgOSe5+wQZHcm55AnBD5fxBftIT/lcv9F+QIfgceQJ5Qf8p79HhTKDs94P6SH63O9V5tIP7vMwE/giTTKCAF2yOMftIjId3Ya+MEChfxfhuAnyCDwxi5P6qOe0Cf0UeCfusmP1UGZR9t03eGCInPlpiBGmCIomITaFPIEwqycnfZF+b+ojLOp9vt/jc71LhSeYa+CexDcAtJCntrlHJXtpc1cB1eCDsNN4Lr2e5w9n8THvctPJhSxhjIwsaT7PDxn0RnPY2cvR8TPgPCsJp8umEPmvnadTspIeqCjctZDHv4LGnxkyvLYJ1Y/rk3Ig3mnfvj3e2UIzziTfracKA8K+c7V7yHt7Lxfhs5VZyDqqV0/z0Me8kdH2ALw//hRG1Sf6E8AugtID3gOEI7FpqIQFJ0b8gC+2j4weY4pGuQM8T2ov6cy8GUBE3RML4T8USVR+QCN51wHXmKxeE1ZHPzwDnzxavQnegYfn+uej1sYADyrVOBbeyLhiP4EfstAeEyZ8YU8eY6UnToDwKRRFNOUEa/6g7V32w99tY+vBdDpQALLYPowcZg97tl+S0dDgA6zy34WPtoN5hizGQDBxjfug6YPA4LyScdEY0YpBwpml/chvlXF9MID5pc6eAeglZef/p8bILQx736oOqk7/CdhlBncIdqAtYEHzpRFue4+qBA3/zrTNnj9VGXWdgECj6TznP9XNWJAF1XlVl12XLEcpX+1wf61ADogWbFlXxSAEtQpQExXAMj9sLzFtu3QR975c9dstbhHJtndj+RZh/75dnevSZbWZ7QVrnrGgbJ+617rMzLXkns+Zqm9RlrnHo/Z+II1/j/5vSmUdFeglq5gM0FBbdawAg+SHhy3xGat3u1+7e63jnn5KYPz7c8PTbBOfadaioJQAkwGYzSdF33ChwsBqIcUbrTfdRtqb2kkMbBmisduCj7v7jnKOogSHplgo+c/4R+DA/JZ61+0BNX7+26P210PjLW773/cA8Q97570Nu7VqOmmoDKu1xjROJcDge3uw595G7E2/GqWg93nCwVwJ91/hY+vBdDRpKPmbbBhc5+0rYc+se2HPrYtr35ia15436aW7rI/9hjumm3Y3DU2ZM4aW//yZ7Z2/ye2cushe273q/b6+0fsxQ8q7O77Btu63W/ZK0LUIanPne+esgfGLLTF29+3nYpa73lghD398oe29fXP7NkDH9mzBz+1BU+/Yp16jrYnXzlqz0t9A8KS/Udt48HPbdfbJ23P4aO296B0MBMCx1SwQHVcl2hnQPvnAfNEBb6cvue943Z39yG2atc7tvf9ctsndG85XGY5Q2dZful2D5x/nT7Q1r3yue3XzcFPT9n+98otd8km6zN1mWv6pL6TbMGmV2z3B9Uqo9L2qZzC1Vusa98R9rFw/am0Avob5aDbyJ1hrvwrfnwtgE7HjV+2Q4D8wDc/4a6gHYOZj+s/3XbqZuSCJ235znfdXSEdk+4ugfJvfKPM7pu41AdEmF3gXHqgzIpfOGoHhIXEPuNdC6MZw29VAtiCdS/YY3OetM16oeukNf4FDC4L5cMDoPIPeqvElYAOwNZtO2AdHxztP+PRaehCOySL8dyrH1q/aau8XgheKGvhzg9t6KzVdlCv39N7Rs03oRB87tWfB0bP9/v43mP9PepnYGCJ0PSdu/WpiUWcbzER/vdpwP4VV+hfH6APmf+8zXv+L77wQueh5elgOr/LyLm29d0quQnrrM/MtTbzmbcsd/P7lvfUYSvY8JK9KETsEIJ/c/94195bpcmfEfI2y5l/WSjgV2+3C7Gp/SY6UCifg8COPTSb971jQwqe9g1av+1baHnbj9msbZ9Y4dYPrXDzm7bpJXnsIKn8hG+B4PLVtz+2vXI3mDqLf3SKvSPkvfz2Z5Y5ZIH/HEb4tV1o8LK9Niz/KTsgpP7mkfk2ZvMRW/BSua163ax4X5U9nLvBJhSudSCv3XXY3tagBOiBsBbJPYfU/EfGAdi16at+fC2ADvhGLN5tc7d84ACn5wi6SGfba+/pq2yjVNvwBU9Y4qOTrfv4Yrtvcqk9OKXUeo2Zb1ukKgHW7C1vWeqjkyz5oRGWLVAn9p5gv0gbbIW7PvWfnsgZkudgigI3oYdZDv19Yudr1m/Gav8v0P+t+yS7N+9p6z5ljT2Uu8oembzcClY9F0OUcqPRhTRmcCiLgcii1ccnKv0HkDo9PNX+9725dlevXLu7T679230T7K5+s2yAykJT3zN4qf9fSNmTS6yXBm238aXy56fYgnV7/PtXrMiybYdt+PyNNrTwKbtv9Dz7U9cBNmvpk64Q4Jf9Mr6nR3en/zOsr/bxtQH640t22ZyNb/pytW9Cqoy22TJ7kTJyof+sBv556Y7ov2YBZDxnqwrnwwLJXkWU7G4EDGi+j/Rs/aEq6zJlnW2TC5CqIJeZi+jXXKO5dILQFTvetqFz19tzquy+sQvdnaAOygVGPkXouyTLztiFyEwIwGTl971j1T41ihWS+2/73jxi+9/93Lemrtz3iY2cXmIvSlVnPFZQ4xaFrROHxETnXlP8+84eE/jfMhbYzNU7bfmzB2zttlft4IfHIz5EzkpVVTTT4hvY1CIWE///rMv//QeAGjL/WVv2whHfEBVmNgAh+67/s/dUO6jr/rNWWvHm6L/4gwB72OOx7fAxn52gLIDI1B/+d+nLJ63b5HX+y7BJg2f5e+TBRw8aedTi52322t22/fBJe3BMofvGAehsqaU8Bh4a3feN6BZoAVLKy+j9uL13pNJ2vfKWDZ605PS7IrT4nCf3Wd7Cp2z/O1WWPSjfBy+8hfIPCvWdH5nqvvufHprkA8H9dPEYBnVY8Dp6gv+6R0xgXVAIFXr6/4H+1Tjo8McXb7cO/eba0Pw1NmrOKhs8c4UNmFFiCf2m2v0CD+B4dPZaSxi2wLKmrLeMqRuty9QnbFD+elv+3CF7Xf395+xHbNDk+TZS745f8JQNyCu1P/QYbfmb33Ef/tddR9gDU1dY31lrbMiCTfZw3mrrMnaZ/TpjkB0Ware//on9qfswT79vUonOa62HXIzHZpc6j1iAcJwsq3Cwfyj8Pzxskh1+50P76GiF/SFjoD00eZWNWbjF+kxbaQ/KZflV2qO29eBndkg8/Lb7WOs2ZbXast4GzllrvaaUWNzDE2zYzCiI/Z9JD7vbMlgxw7D5m2zEvCfl36+yvEXrPEhnkEXbfKvltrhW0BiEk6/28bUAOt207e0yK97+rs15Yq+t2vWWLd32hs3dsMenA5n5QHNuOXjEinZ84D+rMHvPSZu59S8275lXbMdrn7j2fOtIlS1+YqcteXqfLdiwz9bses92v1vtrgizNMt2vGXznn3Npj+136Y/+bIVPveWrdjzkR0UyNGa78t6LNz0suVvfNXyN71hMze9abOefdeW7Ih+45vAtTbYAR3vbdpzwMrKq93r53+5WLT1bSvc9JrN3XjQz7ukntHM0F6p8lmbD6v+A7bg+cPKc8CePvCZa3jKevKFN23J5gNWtOlVW7r9sC169qAte+6AlWza64MNre67E3UmjgHy8FGbr6/i8bUAOp3kHRgjps0+Ffq5Zvtt6MgTFdF/+Qjo8G8jPcZTIBZ1PPnofFl43z5wXGdy4MrgFoU6juia9yH2jjM3Tjpg4ysY6oAYZICQMnEZOFMbZ/KHfNThe9FFlOHvKmN4Tl7iA8oKe3LYoBemOeEjzJFD4T3yhOcQe19qXwfw6/IrfXwtgO7+pQIrftq42v+HuNodp2cEguErKfdFyS8E8OVPpXQ9e/WrFVxWVTv5PhCyoPrI7oXJ1PM1ksNUeStP1HzIwWOAS+nMpvjek0qVrfpI41k0yxHN0hypjD448WtFqp4nVg/pxzUgOUN8XAQ7vHuCj0d0RX72h5PMb9yT4mkinvOfL4Q9OVgRL0flExSHfOy54U2IAeZVfIWPrwnQgQGfqVXbyeroEy8Ad5yepQfZbchvt9P5Vaf8x+RDh5fHwAcBRbKzIn78WJQfKhMq/D+G8kMBJR9Q6z0OUqmG/40BMAEqL42vYio+0/hC93KP7ox4Y7hE70eDxgcBy/HylcNvf8MPXxV5BUwH8mmf3qysiObhTyhD1Dx8bcqWz3/yc1mNiAN/TX84R18jaRAz08IKbe2BX/NfcPLWV/Uw+z8lXpQwqDQugAAAAABJRU5ErkJggg=="
                                    class="fr-fic fr-dii">
                                </td>
                                <td>
                                    <table style="width: 600px; letter-spacing: -1px ; vertical-align: middle">
                                        <tbody>
                                            <tr style="font-size: 28.0px; color: #3076b3;">
                                                <td style="text-align: right; width: 50%;">
                                                    <p style="margin: 0 0 0 0;">
                                                    Zegla Indústria de Máquinas para Bebidas Ltda</p>
                                                </td>
                                            </tr>
                                            <tr style="color: #292c2e; font-size: 16.0px">
                                                <td style="text-align: right; width: 50%;">
                                                    <p style="margin: 0 0 0 0">Travessa José Serafim Fedatto, 277 - Borgo, Bento Gonçalves - RS</p>
                                                </td>
                                            <tr style="color: #292c2e; font-size: 16.0px">
                                                <td style="text-align: right; width: 50%;">
                                                    <p style="margin: 0 0 0 0">Fone: (54)3455-3868 / 991830829 - <a href="mailto:atecnica@zegla.com.br">atecnica@zegla.com.br</a></p>
                                                </td>
                                            </tr>
                                            <tr style="color: #292c2e; font-size: 16.0px">
                                                <td style="text-align: right; width: 50%;">
                                                    <p style="margin: 0 0 0 0"><a href="http://www.zegla.com.br">www.zegla.com.br</a></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 28.0px; color: #3076b3;">
                <td style="text-align: center;"><strong>RELATÓRIO DE ATENDIMENTO TÉCNICO</strong></td>
            </tr>`
            
        l_wDados_Atendimento = `
            <tr>
                <td>
                    <hr>
                    <table style="margin: 0 5% 0px 5%; width: 90%; background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; letter-spacing: -1px; border-collapse: collapse; border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
                        <tbody>
                            <tr>
                                <td style="text-align: center;">
                                    <p style="margin: 0 0 0 0; font-size: 16.0px; color:rgb(31,78,121);"><span style="letter-spacing: -1px;"><strong>DADOS DO ATENDIMENTO</strong></span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%;"><strong>Antedimento</strong></td>
                                <td style="width:56%;">${c_numeroAtendimento}</td>
                                <td style="width:11%; text-align: right;"><strong></strong></td>
                                <td style="width: 22%; text-align: right;"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%;"><strong>Data Início</strong></td>
                                <td style="width:56%;">${c_dataInicio}</td>
                                <td style="width:11%; text-align: right;"><strong>Hora Início</strong></td>
                                <td style="width: 22%; text-align: right;">${c_horaInicio}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%;"><strong>Nr OS</strong></td>
                                <td style="width:56%;">${c_numeroOs}</td>
                                <td style="width:11%; text-align: right;"><strong>Técnico</strong></td>
                                <td style="width:22%; text-align: right;">${c_nomeTecnico}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>`
            
        l_wCliente = `
            <tr>
                <td>
                    <hr>
                    <table style="margin: 0 5% 0px 5%; width: 90%; background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; letter-spacing: -1px; border-collapse: collapse; border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
                        <tbody>
                            <tr>
                                <td style="text-align: center;">
                                    <p style="margin: 0 0 0 0; font-size: 16.0px; color:rgb(31,78,121);"><span style="letter-spacing: -1px;"><strong>INFORMAÇÕES DO CLIENTE</strong></span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%;"><strong>Cliente</strong></td>
                                <td style="width:56%;">${c_nomeCliente}</td>
                                <td style="width:11%; text-align: right;"><strong>Cidade/UF</strong></td>
                                <td style="width: 22%; text-align: right;">${c_cidadeCliente}/${c_estadoCliente}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%;"><strong>Responsável</strong></td>
                                <td style="width:56%;">${c_responsavelCliente}</td>
                                <td style="width:11%; text-align: right;"><strong>Cargo</strong></td>
                                <td style="width: 22%; text-align: right;">${c_cargoResponsavel}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%;"><strong>Telefone</strong></td>
                                <td style="width:56%;">${c_telefoneResponsavel}</td>
                                <td style="width:11%; text-align: right;"><strong>E-mail</strong></td>
                                <td style="width: 22%; text-align: right;">${c_emailResponsavel}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>`
            
        l_wEquipamento = `
            <tr>
                <td>
                    <hr>
                    <table style="margin: 0 5% 0px 5%; width: 90%; background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; letter-spacing: -1px; border-collapse: collapse; border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
                        <tbody>
                            <tr>
                                <td style="text-align: center;">
                                    <p style="margin: 0 0 0 0; font-size: 16.0px; color:rgb(31,78,121);"><span style="letter-spacing: -1px;"><strong>DADOS DO EQUIPAMENTO / LINHA</strong></span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%;"><strong>Tipo</strong></td>
                                <td style="width:56%;">${c_tipoEquipamento}</td>
                                <td style="width:11%; text-align: right;"><strong>PV/Proposta</strong></td>
                                <td style="width: 22%; text-align: right;">${c_pvProposta}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%;"><strong>Modelo</strong></td>
                                <td style="width:56%;">${c_modeloEquipamento}</td>
                                <td style="width:11%; text-align: right;"><strong>Nro. Série</strong></td>
                                <td style="width: 22%; text-align: right;">${c_nroSerieEquipamento}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%; vertical-align: top;"><strong>Descrição do Chamado</strong></td>
                                <td style="width:89%;text-align: justify;">${c_descricaoChamado}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>`
            
        l_wServicos = `
            <tr>
                <td>
                    <hr>
                    <table style="margin: 0 5% 0px 5%; width: 90%; background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; letter-spacing: -1px; border-collapse: collapse; border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
                        <tbody>
                            <tr>
                                <td style="text-align: center;">
                                    <p style="margin: 0 0 0 0; font-size: 16.0px; color:rgb(31,78,121);"><span style="letter-spacing: -1px;"><strong>DESCRIÇÃO DOS SERVIÇOS</strong></span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%; vertical-align: top;"><strong>Sintoma</strong></td>
                                <td style="width:89%; text-align: justify;">${c_sintomaAtendimento}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%; vertical-align: top;"><strong>Causa</strong></td>
                                <td style="width:89%; text-align: justify;">${c_causaAtendimento}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%; vertical-align: top;"><strong>Solução</strong></td>
                                <td style="width:89%; text-align: justify;">${c_solucaoAtendimento}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>`
            
        l_wPecas = `
            <tr>
                <td>
                    <hr>
                    <table style="margin: 0 5% 0px 5%; width: 90%; background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; letter-spacing: -1px; border-collapse: collapse; border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
                        <tbody>
                            <tr>
                                <td style="text-align: center;">
                                    <p style="margin: 0 0 0 0; font-size: 16.0px; color:rgb(31,78,121);"><span style="letter-spacing: -1px;"><strong>PEÇAS APLICADAS / SOLICITADAS</strong></span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                </td>
            </tr>`
            
        l_wEncerramento = `
            <tr>
                <td>
                    <hr>
                    <table style="margin: 0 5% 0px 5%; width: 90%; background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; letter-spacing: -1px; border-collapse: collapse; border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
                        <tbody>
                            <tr>
                                <td style="text-align: center;">
                                    <p style="margin: 0 0 0 0; font-size: 16.0px;color:rgb(31,78,121);"><span style="letter-spacing: -1px;"><strong>ENCERRAMENTO DO ATENDIMENTO</strong></span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tbody>
                            <td style="width:100%; "><strong>Siutação do equipamento após o atendimento</strong></td>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <td style="width:3%; border-style: solid; border-width: 1px; text-align: center; vertical-align: center;"><strong>${c_idSituacaoEquipamento1}</strong></td>
                            <td style="width:30%;">Equipamento 100%</td>
                            <td style="width:3%; border-style: solid; border-width: 1px; text-align: center; vertical-align: center;"><strong>${c_idSituacaoEquipamento2}</strong></td>
                            <td style="width:31%;">Equipamento com Restrição</td>
                            <td style="width:3%; border-style: solid; border-width: 1px; text-align: center; vertical-align: center;"><strong>${c_idSituacaoEquipamento3}</strong></td>
                            <td style="width:30%;">Equipamento Parado</td>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:11%; vertical-align: top;"><strong>Comentários Técnico</strong></td>
                                <td style="width:89%; text-align: justify;">${c_comentariosTecnicoAtendimento}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: right;">
                        <tbody>
                            <tr>
                                <td rowspan="4" style="width:70%;"></td>
                                <td style="width: 30%;">
                                    <tr>
                                    <td style="text-align: center;">${c_nomeTecnico}</td>
                                    </tr>
                                    <tr>
                                    <td style="border-top: 1px solid #333333;"></td>
                                    </tr>
                                    <tr>
                                    <td style="text-align: center;"><strong>Assinatura do Técnico</strong></td>
                                    </tr>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>`
            
        l_wCustos = `
            <tr>
                <td>
                    <hr>
                    <table style="margin: 0 5% 0px 5%; width: 90%; background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; letter-spacing: -1px; border-collapse: collapse; border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
                        <tbody>
                            <tr>
                                <td style="text-align: center;">
                                    <p style="margin: 0 0 0 0; font-size: 16.0px;color:rgb(31,78,121);"><span style="letter-spacing: -1px;"><strong>CUSTOS ENVOLVIDOS</strong></span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 800px; margin-left: auto; margin-right: auto;">
                        <tbody>
                            <tr>
                                <th style="width:34%; text-align: left; background-color: #3076b3; color: #fff;height: 24px; font-size: 14px;">Dados do Serviço</th>
                                <th style="width:33%; text-align: center; background-color: #3076b3;  color: #fff;height: 24px; font-size: 14px;"><strong>Horas Normais</strong></th>
                                <th style="width:33%; text-align: right; text-align: center; background-color: #3076b3;  color: #fff;height: 24px; font-size: 14px;"><strong>Horas Extras</strong></th>
                            </tr>
                            <tr>
                                <td style="width:34%; text-align: left; border-bottom: 1px solid #f1f1f1;border-collapse: collapse;"><strong>Horas de Viagem</strong></td>
                                <td style="width:33%; text-align: center;border-bottom: 1px solid #f1f1f1;border-collapse: collapse;">${l_totalHorasViagemString}</td>
                                <td style="width:33%; text-align: right; text-align: center;border-bottom: 1px solid #f1f1f1;border-collapse: collapse;">${c_totalHorasViagemExtraString}</td>
                            </tr>
                            <tr>
                                <td style="width:34%; text-align: left;border-bottom: 1px solid #f1f1f1;border-collapse: collapse;"><strong>Horas de Serviço</strong></td>
                                <td style="width:33%; text-align: center;border-bottom: 1px solid #f1f1f1;border-collapse: collapse;">${l_totalHorasServicoString}</td>
                                <td style="width:33%; text-align: right; text-align: center;border-bottom: 1px solid #f1f1f1;border-collapse: collapse;">${c_totalHorasServicoExtraString}</td>
                            </tr>
                            <tr>
                                <td style="width:34%; text-align: left;border-bottom: 1px solid #f1f1f1;border-collapse: collapse;"><strong>Km Rodados</strong></td>
                                <td style="width:33%; text-align: center;border-bottom: 1px solid #f1f1f1;border-collapse: collapse;">${l_totalKmAtendimento}Km</td>
                                <td style="width:33%; text-align: center;border-bottom: 1px solid #f1f1f1;border-collapse: collapse;"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>`
            
        l_wAvaliacaoCliente = `
            <tr>
                <td>
                    <hr>
                    <table style="margin: 0 5% 0px 5%; width: 90%; background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; letter-spacing: -1px; border-collapse: collapse; border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
                        <tbody>
                            <tr>
                                <td style="text-align: center;">
                                    <p style="margin: 0 0 0 0; font-size: 16.0px;color:rgb(31,78,121);"><span style="letter-spacing: -1px;"><strong>AVALIAÇÃO DO CLIENTE</strong></span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:20%; text-align: left;"><strong>Pendências</strong></td>
                                <td style="width:80%; text-align: left;">${c_possuiPendencias}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:20%; text-align: left;"><strong>Atendimento Satisfatório</strong></td>
                                <td style="width:80%; text-align: left;">${c_atendimentoSatisfatorio}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="width:20%; text-align: left; vertical-align: top;"><strong>Comentários do Cliente</strong></td>
                                <td style="width:80%; text-align: left; text-align: justify;">${c_comentariosClienteAtendimento}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: left;">
                        <tbody>
                            <tr>
                                <td style="text-align: left; vertical-align: top;">Eu, ${c_responsavelAssinatura}, RG/Matricula ${c_docResponsavelAssinatura}, confirmo as informações descritas  acima.</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%; text-align: right;">
                        <tbody>
                            <tr>
                                <td rowspan="4" style="width:70%;"></td>
                                <td style="width: 30%;">
                                    <tr>
                                        <td style="text-align: center;">${c_responsavelAssinatura}</td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: 1px solid #333333;"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;"><strong>Assinatura do Cliente</strong></td>
                                    </tr>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>`
            
        l_wFim = `
                            </tbody>
                        </table>
                    </div>
                </body>
            </html>`
            
        v_relatorio = 
            l_wInicio +
            l_wCabecalho +
            l_wDados_Atendimento +
            l_wInformacoesContato +
            l_wEquipamento +
            l_wServicos +
            l_wPecas +
            l_wEncerramento +
            l_wCustos +
            l_wAvaliacaoCliente +
            l_wFim;
    }

    return v_relatorio
}

// 
// Controle Clicks em coluna de tabela 
$('#' + tabelaAtendimentos + ' tbody').on('click', 'tr', function (obj) {
    var celulaClicada = $(obj.target).closest('td');
    var linhaClicada = $(obj.target).closest('tr');
    if (celulaClicada[0].cellIndex === 10) {
        var registroClicado = linhaClicada[0].rowIndex;
        // Se realizado click no campo BIT_ATENDIMENTO_GARANTIA
        var novoValor = celulaClicada.html() === 'Sim' ? 'False' : 'True';
        var reg = GetRecord(tabelaAtendimentos, registroClicado);
        reg.SetValue('CL_ATENDIMENTOS_ASSIS_TEC.BIT_ATENDIMENTO_GARANTIA', novoValor);

        // Altera Registros de Tempo para Cobrado ou Não
        var v_idAtendimento = $(this).closest('tr').find('td[data-key]').data('key')
        for(i = 1; i <= TableCountRows(tabelaRegistrosTempoChamado);i++){
            let l_obj1 = GetRecord(tabelaRegistrosTempoChamado,i);
            let l_idAtendimentoHorario = l_obj1.GetValue('CL_REGISTROS_HORARIO_CHAMADO.idCL_ATENDIMENTO_ASSIS_TEC')
            if(l_idAtendimentoHorario == v_idAtendimento){
                let l_novoValorInvertido = novoValor === 'True' ? 'False' : 'True'
                l_obj1.SetValue('CL_REGISTROS_HORARIO_CHAMADO.BIT_COBRAR_TEMPO', l_novoValorInvertido)
                l_obj1.SetValue('CL_REGISTROS_HORARIO_CHAMADO.BIT_COBRAR_KM', l_novoValorInvertido)
            }
        }

        //Altera Despesa para Cobrada ou Não
        for(i = 1; i <= TableCountRows(tabelaDespesas);i++){
            let l_obj2 = GetRecord(tabelaDespesas,i);
            let l_idAtendimentoDespesa = l_obj2.GetValue('CL_DESPESAS_CHAMADO.idCL_ATENDIMENTO_ASSIS_TEC')
            if(l_idAtendimentoDespesa == v_idAtendimento){
                let l_novoValorInvertido = novoValor === 'True' ? 'False' : 'True'
                l_obj2.SetValue('CL_DESPESAS_CHAMADO.BIT_COBRAR_CLIENTE', l_novoValorInvertido)
            }
        }

        return false; 
    }
    // Controle Clicks na Coluna Relatório da Tabela de Atendimentos
    if (celulaClicada[0].cellIndex == 9 && celulaClicada.html() == 'Relatório Atendimento') {
        let l_idAtendimento = $(this).closest('tr').find('td[data-key]').data('key')
        let l_desNumeroAtendimento = $(this).closest('tr').find('td').eq(1).html().trim()
        let l_htmlRelatorio = f_geraRelatorioAtendimento(l_idAtendimento)

        let l_w = window.open('',l_desNumeroAtendimento);
        l_w.document.body.innerHTML = l_htmlRelatorio;	
        l_w.document.title = 'Conectado - Rel. '+l_desNumeroAtendimento;

    }

});

//Todo: Cria resumo de campo Varchar(Max) para table
OnChange(causa, function(){
    let l_causa = GetValue(causa)
    let l_resumoCausa = l_causa.length > 47 ? l_causa.substring(0,46)+'...' : l_causa
    SetValue(resumoCausa, l_resumoCausa)
})


//Espaços HTML
&nbsp; //*1 espaço
&ensp; //*2 espaços
&emsp; //*3 espaços

/*Usado para mostrar o chat do Pedrinho para conversar com o cliente */
(new function()
{
    const me = this;

        me.GetIframe = function(){
        $.ajax({
            url: "/FormularioDinamico/SuporteChat/GetChat",
            type: "POST",
            data:
                {
                codInstancia: $('#codInstancia').val()
            },
            success: function(response){
                    me.iframe = $(response);
                $('body').append(me.iframe)
             }
            })
    }

        me.GetIframeMessages = function(element) {
            var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
            var eventer = window[eventMethod];
            var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

            eventer(messageEvent, function(e) {
                if (e.data.command == "SIZE_CHANGED")
                {
                    me.iframe.width(e.data.width)
                       me.iframe.height(e.data.height)
                    }
            }, false);
        }

        me.GetIframeMessages();
        me.GetIframe();
    })

//Centralizar campo no form
$('#'+devolverEquipe+'_Check').append('Devolver acompanhamento para equipe?')
$('#'+devolverEquipe+'_Check').css({"width":"65%"})
$('#'+devolverEquipe+'_Check').css({"text-align":"center"})

//centralizar button no form
$("#"+incluirAtendimento).css({
    'margin-left' : '200%'
})

//Todo: CSS para App
/*Ajuste de CSS para APP*/
{
    /*--------------------------------------------------------------------------------------------*/
    //Cabeçalho
    $('#'+formCabecalhoAtividade).css({
        'margin-right':'10px',
        'margin-left':'50px',
    })
    //Redução de Padding de Layouts
    $('#Layout220210209110428942_parent_direita').css({
        'padding-left': '20px'   
    })
    $('#Layout220210209110428942_parent_esquerda').css({
        'padding-left': '20px'   
    })
    $('#Layout220210209110430683_parent_direita').css({
        'padding-left': '20px'   
    })
    $('#Layout220210209110430683_parent_esquerda').css({
        'padding-left': '20px'   
    })

    //Geral
    $('.tab-pane').css({
        'padding-left': '40px'   
    })

    $('.campo-form-dinamico').css({
        'padding-right': '0px'   
    })

    $('.panel-form-dinamico').css({
        'padding-right': '0px',
        'padding-left': '0px'
    })

    $('.tab-content').css({
        'padding-top': '15px',
        'padding-bottom': '200px'
    })
    $('.modal-body').css({
        'padding-right': '0px',
        'padding-bottom': '200px'
    })


    //Registros de Tempo
    //Redução de Padding de Panel
    $('#20201229183707288_parent').css({
        'padding-left': '10px' ,
        'overflow': 'auto'   
    })
    //Ajuste de margem de Form Reusável
    $('#divConfiguracaoPrincipalReusableForms20201229184031672').css({
        'margin-left': '-82px'   
    })
    // Form Tabela Registro Tempo
    $('#20201218092358257_parent').css({
        'padding-left': '10px'   
    })
    $('#20201218093021316_parent').css({
        'padding-left': '10px'   
    })
    $('#20201218093104358_parent').css({
        'padding-left': '10px'   
    })
    $('#20201218093127179_parent').css({
        'padding-left': '10px'   
    })     

    //Inicio Deslocamento
    $('#20201218144833883_parent').css({
        'padding-left': '10px'   
    })
    $('#divConfiguracaoPrincipalReusableForms20201229181843882').css({
        'margin-left': '-82px'   
    })  

    //Cliente
    $('#Layout220201217163637150_parent_direita').css({
        'padding-left': '10px'   
    })
    $('#Layout220201217163637150_parent_esquerda').css({
        'padding-left': '10px'   
    })

    //Ordem Serviço
    $('#Layout220201217163527073_parent_direita').css({
        'padding-left': '10px'   
    })
    $('#Layout220201217163527073_parent_esquerda').css({
        'padding-left': '10px'   
    })

    //Endereço Cliente
    $('#Layout220201217163800213_parent_direita').css({
        'padding-left': '10px'   
    })
    $('#Layout220201217163800213_parent_esquerda').css({
        'padding-left': '10px'   
    })

    //Dados Chamado
    $('#20201217171006397_parent').css({
        'padding-left': '10px'   
    })

    //Descrição Chamado
    $('#Label20201217171245744').css({
        'padding-left': '10px'   
    })

    //Dados do Equipamento Simples
    $('#20201217171424667_parent').css({
        'padding-left': '10px'   
    })

    //Dados Equipamento Multiplo
    $('#20210514150332816_parent').css({
        'padding-left': '10px'   
    })

    //Historico Equipamento
    $('#20201217171643648_parent').css({
        'padding-left': '10px'   
    })

    //Filtro Historico
    $('#Label20210303071807504').css({
        'padding-left': '30px'   
    })
    $('#20210303071807504').css({
        'padding-left': '30px'   
    })

    //Orientações
    $('#20201217172000081_parent').css({
        'padding-left': '20px',
        'overflow': 'auto'    
    })
    //Form Tabela Orientações
    $('#20201217172056165_parent').css({
        'padding-left': '10px'   
    })

    //Despesas
    $('#20201230142258099_parent').css({
        'padding-left': '20px',
        'overflow': 'auto'    
    })
    //Form Tabela Despesa
    $('#20201217172713580_parent').css({
        'padding-left': '10px'   
    })
    

    //Demandas
    $('#20201230180257313_parent').css({
        'padding-left': '20px',
        'overflow': 'auto'    
    })    
    //Form Tabela Demanda
    $('#20201217174725620_parent').css({
        'padding-left': '10px'   
    })
    $('#202012171751178791').css({
        'padding-left': '0px',
        'margin-left': '-40px'
    })
    $('#Tabs202012171751178798').css({
        'padding-right': '0px'   
    })
    $('#20201217175140590_parent').css({
        'padding-left': '10px'   
    })
    $('#202012171751178792655').css({
        'padding-left': '10px',
        'margin-left': '-20px'
    })

    PanelExpanded(panelOrdemServico, false);
    PanelExpanded(panelEnderecoContato, false);
    PanelExpanded(panelCabecalhoChamado, false);
    
    /*-----------------------------------------------------------------------------------*/
}

//Todo: Primeira sessão de regras para reconstruir campos checkbox com switch
//! Usar sempre no início das regras pois se um evento OnChange for programado sobre o campo ele será perdido na recostrução.
function f_SwitchCheckBox(campo) {
    let checkID = '#' + campo + '_Check';
    let checkLabelId = 'Label' + campo;
    let checkLabelText = $('#' + checkLabelId).prop('innerHTML'); // Armazena Texto da Label andtes de eliminá-la
    let checkLabel = document.getElementById(checkLabelId);
    checkLabel.remove(); // Remove Label do campo já que será inserida junto ao CheckBox com Switch
    let checkValue = GetValue(campo); // Guarda valor do campo antes de sua reconstrução
    let switchPre = '<label class="conectado-switch-outer-label"> <span>' + checkLabelText + '</span> <label class="conectado-switch">';
    let switchPos = '<span class="conectado-slider round"></span></label></label>';
    let checkField = $(checkID).prop('innerHTML');
    $(checkID).prop('innerHTML', switchPre + checkField + switchPos);
    $(checkID).css('text-align', 'center');
    SetValue(campo, checkValue); // Retorna valor ao campo apos sua reconstrução
}

f_SwitchCheckBox(ProvisoesGerenciamento_NecessitaPecas);
  //Todo: Término sessão de conversão dos campos checkbox para switch



  //Todo Rodar Função após Loading da Página

function f_rodarDepois(){
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(/*Sua função aqui*/)
        },500)
    })
}

f_rodarDepois()


//Todo Consulta de Fluxo dentro de Form
//-------------------------------------------
//Todo: Função que Utiliza Consulta de Atividades do Conectado para mostrar no formulário atividades ativas do processo
function Fluxos_Revisoes_Custos(idTableQuery) {
    //* Função interna que adiciona resultados da consulta para uma instância após um determinado elementoHTML
    function consultaAtividade(elementoHTML, codInstancia, codStatus) {
    let strCommand =
        'http://' +
        window.location.host +
        '/Consulta/ConsultaAtividades/Index?pesquisaInstancia=' +
        codInstancia +
        '&pesquisaAtividadeStatus=' +
        codStatus +
        '&X-Requested-With=XMLHttpRequest&_=#';
    let json = $.getJSON(strCommand);
    let Temporizar_aguarda_resposta_da_consulta = setTimeout(function () {
        $(elementoHTML).append($(json.responseText));
    }, 1000);
    }

    let wTQ = $('#TableQuery' + idTableQuery); // Captura elemento da TableQuery vazia utilizada para ser substituida
    let elementoHTML = wTQ.parent(); // Captura o elemento de referência, após o qual se adiciona os resultados da consulta
    $(wTQ).remove(); // remove o elemento da TableQuery
    let widCL_CHAMADO = GetThisWorkFlowInfo().codRegistroEntidade; // Captura idCL_CHAMADO
    let reg = SqlQuery(`select ac.des_id_atendimento "idAtendimento"
                                , rc.num_fluxo          "NUM_FLUXO"
                            from cl_revisoes_custo_atendimento rc
                                , cl_atendimentos_chamado ac
                            where isnull(rc.bit_ax_pedir_correcao_registro_custo,1) = 1 
                            and isnull(rc.tip_excluido,0)=0
                            and rc.idcl_atendimento_chamado = ac.idcl_atendimento_chamado
                            and isnull(ac.tip_excluido,0)=0
                            and ac.idcl_chamado=${widCL_CHAMADO}`); // Select que pega os números de fluxo das instâncias
    let fluxos = reg.entity; // Armazena os resultado em um Array
    fluxos.forEach((element) => consultaAtividade(elementoHTML, element.NUM_FLUXO, 1)); // Para cada Fluxo executa a Constulta de Atividades.
}

  //* Realiza consulta de Fluxos de Revisoes de Custos Ativos
Fluxos_Revisoes_Custos(RevisarConclusaoChamado_AtendimentoscomRevisaodeCustosnaoConcluida);

  //-------------------------------------------


//Todo pegar cores do quadro de kanban
    // Function
        function f_insertCoresKanban(){
            var v_div = document.createElement('div')
            v_div.id = 'seletorCoresKanbanDiv'
            v_div.innerHTML = `
                <div class="seletor-cores">
                    <label class="conectado-padrao seletor-cores-opcao">
                        <input checked="checked" data-classecss="conectado-padrao" data-corfontehexa="#2d2d2a" data-corhexa="#ecf0f1" data-title="Padrao" name="cor" type="radio" value="Padrao">
                        <div class="button" title="Padrao">
                            <span><i class="fa-check fa"></i></span>
                        </div>
                    </label>
                    <label class="conectado-verde-claro seletor-cores-opcao">
                        <input data-classecss="conectado-verde-claro" data-corfontehexa="#fbfefa" data-corhexa="#70b77e" data-title="Verde Claro" name="cor" type="radio" value="VerdeClaro">
                        <div class="button" title="Verde Claro">
                            <span>
                            <i class="fa-check fa"></i>
                            </span>
                        </div>
                    </label>
                    <label class="conectado-azul-escuro seletor-cores-opcao">
                        <input data-classecss="conectado-azul-escuro" data-corfontehexa="#fbfefa" data-corhexa="#0c6291" data-title="Azul Escuro" name="cor" type="radio" value="AzulEscuro">
                        <div class="button" title="Azul Escuro">
                            <span>
                            <i class="fa-check fa"></i>
                            </span>
                        </div>
                    </label>
                    <label class="conectado-turquesa seletor-cores-opcao">
                        <input data-classecss="conectado-turquesa" data-corfontehexa="#fbfefa" data-corhexa="#368f8b" data-title="Turquesa" name="cor" type="radio" value="Turquesa">
                        <div class="button" title="Turquesa"><span><i class="fa-check fa"></i></span></div>
                    </label>
                    <label class="conectado-laranja seletor-cores-opcao">
                        <input data-classecss="conectado-laranja" data-corfontehexa="#fbfefa" data-corhexa="#ef7b45" data-title="Laranja" name="cor" type="radio" value="Laranja">
                        <div class="button" title="Laranja"><span><i class="fa-check fa"></i></span></div>
                    </label>
                    <label class="conectado-amarelo seletor-cores-opcao">
                        <input data-classecss="conectado-amarelo" data-corfontehexa="#fbfefa" data-corhexa="#eeb902" data-title="Amarelo" name="cor" type="radio" value="Amarelo">
                        <div class="button" title="Amarelo"><span><i class="fa-check fa"></i></span></div>
                    </label>
                    <label class="conectado-vermelho seletor-cores-opcao">
                        <input data-classecss="conectado-vermelho" data-corfontehexa="#fbfefa" data-corhexa="#e03616" data-title="Vermelho" name="cor" type="radio" value="Vermelho">
                        <div class="button" title="Vermelho"><span><i class="fa-check fa"></i></span></div>
                    </label>
                    <label class="conectado-preto seletor-cores-opcao">
                        <input data-classecss="conectado-preto" data-corfontehexa="#fbfefa" data-corhexa="#2e282a" data-title="Preto" name="cor" type="radio" value="Preto">
                        <div class="button" title="Preto"><span><i class="fa-check fa"></i></span></div>
                    </label>
                    <label class="conectado-branco seletor-cores-opcao">
                        <input data-classecss="conectado-branco" data-corfontehexa="#2d2d2a" data-corhexa="#fcfffa" data-title="Branco" name="cor" type="radio" value="Branco">
                        <div class="button" title="Branco"><span><i class="fa-check fa"></i></span></div>
                    </label>
                    <label class="conectado-cinza seletor-cores-opcao">
                        <input data-classecss="conectado-cinza" data-corfontehexa="#fbfefa" data-corhexa="#4c4c47" data-title="Cinza" name="cor" type="radio" value="Cinza">
                        <div class="button" title="Cinza"><span><i class="fa-check fa"></i></span></div>
                    </label>
                </div>
            `
            document.getElementById(panelSeletorCores+'_target').appendChild(v_div)
            $('#seletorCoresKanbanDiv').css({
                'width':'300px','margin-left':'auto','margin-right':'auto','overflow':'auto'
            })
        }
    // Loading
    f_insertCoresKanban()
    if(!HasValue(corQuadroKanban)){
        SetValue(corQuadroKanban,'Padrao')
    }

    // Click
    $('#seletorCoresKanbanDiv ').click(function(){
        SetValue(corQuadroKanban,$("input[name='cor']:checked").val())
})



//Todo Preencher Select2 
function preencheDropDown(sendUrl, parentSelector, childSelector) {
    if ($(parentSelector).val() != "" && $(parentSelector).val() != null) {
        $(childSelector).children().remove();
        $.ajax(sendUrl, {
            method: "POST",
            data: { id: $(parentSelector).val() },
            success: function (response) {
                $(childSelector).append('<option value>' + campos.selecionar + '</option>');
                $(response).each(function (index, element) {
                    $(childSelector).append('<option value="' + element.Value + '">' + element.Text + '</option>');
                });
                $(childSelector).trigger("change");
            },
            error: function () { alertify.error(mensagens.ajaxErro); }
        });
    }
};

// Todo Capturar Evento Redimensionamento Janela
window.onresize = function() {
    //function here
};


//Todo: Validação de Formulário
    //Validar conteúdo de formulário pemitindo concluir a atividade ou mostrar mensagem de erro/alerta para o usuário
    ValidationMessage('mensagem','boolean (opcional)')
    //'mensagem' = mensagem a ser mostrada. A mensagem impede a finalização da atividade.
    //'boolean (opcional)' = true ou false para determinar refresh da página após. Parametro opicional, quando true força refresh na página após clicar em ok. 
    //Necessário para quando ValidationMessage precisa usar savemodel() devido a problema na duplicação de registros de collection*/
    
    //Ex.:
    var msg = 'Teste'
    ValidationMessage(msg,true);
        
    //Regras pra usar em conjunto com Validation quando precisa chamar WS externo na saída da atividade
        //Desabilita o controle de encerrar a atividade , apenas valida campos Obrigatórios  e regras , mas não encerra a atividade */
        DisableFormControl();
        //Mostra a tela processando */
        loading();
        //Comando que manda encerrar a atividade */
        FinishTask();
        //Fecha a tela processando */
        loaded();


//Todo Refresh

    // Refresh assincrono
    RefreshAsync('20180402075809044 , 20180402075809045 , 20180402075809046',function(){});
    

    Refresh('20180328142804459 , 20180328142804460 , 20180328142804461'); 

//Todo Métodos Assincronos
    //Pegar dados do workflow  assincrono
    GetWorkFlowInfoAsync(num_fluxo,function(variavel){/*métodos dependentes*/}); //variaavel = onde dados serão gravados; métodos dependentes = métodos que serão rodados de forma sincrona
    GetThisWorkFlowInfoAsync(function(variavel){/*métodos dependentes*/});

    GetThisTaskInfoAsync(function(dados){
            dados.dtaPrevistaFim
    })

    	// async
	GetUserInfoAsync(function(usuario){
		usuario.id
		usuario.desNome
		usuario.desApelido
	});

    //Filtro Assincrono - Carrega registros de coleçaõ já relacionados previamente*/
    ApplyTableFilterAsync(alias,'idCL_ASSUNTO_CHAMADO = '+GetValue(alias), function(){
        //script here
    });

    
    // Métodos Assincronos	
    //Pegar dados entidades: Método assíncrono. Quando terminar o processamento executa a funcao. Paramentro de entrada da funcao é o nome da variaável de entidade que deseja ex : ent_cliente */	
    GetEntityAsync('nomeEntidadeBanco','condição da clausula WHERE SQL partindo da tabela do campo' , function(entidade){
        entidade.entity[0].Nome_Campo_Banco
        //script here
    });
    //Ex.:
        GetEntityAsync('CL_CLIENTES', 'idCL_CLIENTE = ' + GetValue('20180402155250621') , function(ent_cliente){
            var cod_cliente = ent_cliente.entity[0].COD_CLIENTE_CONECTA;
        });

//Todo: Informações de Instancia, Atividade Execucação e Usuário Logado
    //Pegar Id Atividade Execução Atual*/
    $("#idExecucaoAtividade").val()

//*Todo: Forçar salvamento*/
    // Salvar os dados do form no banco
    SaveModel();

    //Após o savemodel() é necessário dar refresh em todas as collections*/
    Refresh('20190522161453040');

    

    // Salvar os dados do form no banco Assincrono
    SaveModelAsync(function(){
        // faz alguma coisa quando terminar
    }):
        ex.: 
        loading();

        SaveModelAsync(function(){
            loaded();
        });


//Todo Preencher Select2 
function preencheDropDown(sendUrl, parentSelector, childSelector) {
    if ($(parentSelector).val() != "" && $(parentSelector).val() != null) {
        $(childSelector).children().remove();
        $.ajax(sendUrl, {
            method: "POST",
            data: { id: $(parentSelector).val() },
            success: function (response) {
                $(childSelector).append('<option value>' + campos.selecionar + '</option>');
                $(response).each(function (index, element) {
                    $(childSelector).append('<option value="' + element.Value + '">' + element.Text + '</option>');
                });
                $(childSelector).trigger("change");
            },
            error: function () { alertify.error(mensagens.ajaxErro); }
        });
    }
};

//Todo pegar cores do quadro de kanban
    // Function
    function f_insertCoresKanban(){
        var v_div = document.createElement('div')
        v_div.id = 'seletorCoresKanbanDiv'
        v_div.innerHTML = `
            <div class="seletor-cores">
                <label class="conectado-padrao seletor-cores-opcao">
                    <input checked="checked" data-classecss="conectado-padrao" data-corfontehexa="#2d2d2a" data-corhexa="#ecf0f1" data-title="Padrao" name="cor" type="radio" value="Padrao">
                    <div class="button" title="Padrao">
                        <span><i class="fa-check fa"></i></span>
                    </div>
                </label>
                <label class="conectado-verde-claro seletor-cores-opcao">
                    <input data-classecss="conectado-verde-claro" data-corfontehexa="#fbfefa" data-corhexa="#70b77e" data-title="Verde Claro" name="cor" type="radio" value="VerdeClaro">
                    <div class="button" title="Verde Claro">
                        <span>
                        <i class="fa-check fa"></i>
                        </span>
                    </div>
                </label>
                <label class="conectado-azul-escuro seletor-cores-opcao">
                    <input data-classecss="conectado-azul-escuro" data-corfontehexa="#fbfefa" data-corhexa="#0c6291" data-title="Azul Escuro" name="cor" type="radio" value="AzulEscuro">
                    <div class="button" title="Azul Escuro">
                        <span>
                        <i class="fa-check fa"></i>
                        </span>
                    </div>
                </label>
                <label class="conectado-turquesa seletor-cores-opcao">
                    <input data-classecss="conectado-turquesa" data-corfontehexa="#fbfefa" data-corhexa="#368f8b" data-title="Turquesa" name="cor" type="radio" value="Turquesa">
                    <div class="button" title="Turquesa"><span><i class="fa-check fa"></i></span></div>
                </label>
                <label class="conectado-laranja seletor-cores-opcao">
                    <input data-classecss="conectado-laranja" data-corfontehexa="#fbfefa" data-corhexa="#ef7b45" data-title="Laranja" name="cor" type="radio" value="Laranja">
                    <div class="button" title="Laranja"><span><i class="fa-check fa"></i></span></div>
                </label>
                <label class="conectado-amarelo seletor-cores-opcao">
                    <input data-classecss="conectado-amarelo" data-corfontehexa="#fbfefa" data-corhexa="#eeb902" data-title="Amarelo" name="cor" type="radio" value="Amarelo">
                    <div class="button" title="Amarelo"><span><i class="fa-check fa"></i></span></div>
                </label>
                <label class="conectado-vermelho seletor-cores-opcao">
                    <input data-classecss="conectado-vermelho" data-corfontehexa="#fbfefa" data-corhexa="#e03616" data-title="Vermelho" name="cor" type="radio" value="Vermelho">
                    <div class="button" title="Vermelho"><span><i class="fa-check fa"></i></span></div>
                </label>
                <label class="conectado-preto seletor-cores-opcao">
                    <input data-classecss="conectado-preto" data-corfontehexa="#fbfefa" data-corhexa="#2e282a" data-title="Preto" name="cor" type="radio" value="Preto">
                    <div class="button" title="Preto"><span><i class="fa-check fa"></i></span></div>
                </label>
                <label class="conectado-branco seletor-cores-opcao">
                    <input data-classecss="conectado-branco" data-corfontehexa="#2d2d2a" data-corhexa="#fcfffa" data-title="Branco" name="cor" type="radio" value="Branco">
                    <div class="button" title="Branco"><span><i class="fa-check fa"></i></span></div>
                </label>
                <label class="conectado-cinza seletor-cores-opcao">
                    <input data-classecss="conectado-cinza" data-corfontehexa="#fbfefa" data-corhexa="#4c4c47" data-title="Cinza" name="cor" type="radio" value="Cinza">
                    <div class="button" title="Cinza"><span><i class="fa-check fa"></i></span></div>
                </label>
            </div>
        `
        document.getElementById(panelSeletorCores+'_target').appendChild(v_div)
        $('#seletorCoresKanbanDiv').css({
            'width':'300px','margin-left':'auto','margin-right':'auto','overflow':'auto'
        })
    }
// Loading
f_insertCoresKanban()
if(!HasValue(corQuadroKanban)){
    SetValue(corQuadroKanban,'Padrao')
}

// Click
$('#seletorCoresKanbanDiv ').click(function(){
    SetValue(corQuadroKanban,$("input[name='cor']:checked").val())
})

//Todo: Jquery para detecção de mouse
    //tirar  mouse do campo/botão
    $("#20190910165923034").mouseleave(function(){
        //código
        });
        // colocar o mouse sobre o campo/botão
        $("#20190910165923034").mouseenter(function(){
        //código
        });
        // clicar sobre o campo/botão
        $("#20190910165923034").click(function(){
        //código
        });
        // clicar duplamente sobre o campo/botão
        $("#20190910165923034").dblclick(function(){
        //código
        });
        
    
    //Todo: Segmento de Código a adicionar ao formulário para anexar à instância do processo um Chat conectado ao Portal de Cliente da Conecta
    //* Início código integração Chat do Portal de Cliente da Conecta
    (new function()
    {
        const me = this;
    
            me.GetIframe = function(){
            $.ajax({
                url: "/FormularioDinamico/SuporteChat/GetChat",
                type: "POST",
                data:
                    {
                    codInstancia: $('#codInstancia').val()
                },
                success: function(response){
                        me.iframe = $(response);
                    $('body').append(me.iframe)
                 }
                })
        }
    
            me.GetIframeMessages = function(element) {
                var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
                var eventer = window[eventMethod];
                var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
    
                eventer(messageEvent, function(e) {
                    if (e.data.command == "SIZE_CHANGED")
                    {
                        me.iframe.width(e.data.width)
                           me.iframe.height(e.data.height)
                        }
                }, false);
            }
    
            me.GetIframeMessages();
            me.GetIframe();
        })
    //* Fim código integração Chat do Portal de Cliente da Conecta
    
    //Todo: Primeira sessão de regras para reconstruir campos checkbox com switch
    //! Usar sempre no início das regras pois se um evento OnChange for programado sobre o campo ele será perdido na recostrução.
    function f_SwitchCheckBox(campo) {
        let checkID = '#' + campo + '_Check';
        let checkLabelId = 'Label' + campo;
        let checkLabelText = $('#' + checkLabelId).prop('innerHTML'); // Armazena Texto da Label andtes de eliminá-la
        let checkLabel = document.getElementById(checkLabelId);
        checkLabel.remove(); // Remove Label do campo já que será inserida junto ao CheckBox com Switch
        let checkValue = GetValue(campo); // Guarda valor do campo antes de sua reconstrução
        let switchPre = '<label class="conectado-switch-outer-label"> <span>' + checkLabelText + '</span> <label class="conectado-switch">';
        let switchPos = '<span class="conectado-slider round"></span></label></label>';
        let checkField = $(checkID).prop('innerHTML');
        $(checkID).prop('innerHTML', switchPre + checkField + switchPos);
        $(checkID).css('text-align', 'center');
        SetValue(campo, checkValue); // Retorna valor ao campo apos sua reconstrução
    }
    

//Todo: Função que Utiliza Consulta de Atividades do Conectado para mostrar no formulário atividades ativas do processo
function Fluxos_Revisoes_Custos(idTableQuery) {
    //* Função interna que adiciona resultados da consulta para uma instância após um determinado elementoHTML
    function consultaAtividade(elementoHTML, codInstancia, codStatus) {
    let strCommand =
        'http://' +
        window.location.host +
        '/Consulta/ConsultaAtividades/Index?pesquisaInstancia=' +
        codInstancia +
        '&pesquisaAtividadeStatus=' +
        codStatus +
        '&X-Requested-With=XMLHttpRequest&_=#';
    let json = $.getJSON(strCommand);
    let Temporizar_aguarda_resposta_da_consulta = setTimeout(function () {
        $(elementoHTML).append($(json.responseText));
    }, 1000);
    }

    let wTQ = $('#TableQuery' + idTableQuery); // Captura elemento da TableQuery vazia utilizada para ser substituida
    let elementoHTML = wTQ.parent(); // Captura o elemento de referência, após o qual se adiciona os resultados da consulta
    $(wTQ).remove(); // remove o elemento da TableQuery
    let widCL_CHAMADO = GetThisWorkFlowInfo().codRegistroEntidade; // Captura idCL_CHAMADO
    let reg = SqlQuery(`select ac.des_id_atendimento "idAtendimento"
                                , rc.num_fluxo          "NUM_FLUXO"
                            from cl_revisoes_custo_atendimento rc
                                , cl_atendimentos_chamado ac
                            where isnull(rc.bit_ax_pedir_correcao_registro_custo,1) = 1 
                            and isnull(rc.tip_excluido,0)=0
                            and rc.idcl_atendimento_chamado = ac.idcl_atendimento_chamado
                            and isnull(ac.tip_excluido,0)=0
                            and ac.idcl_chamado=${widCL_CHAMADO}`); // Select que pega os números de fluxo das instâncias
    let fluxos = reg.entity; // Armazena os resultado em um Array
    fluxos.forEach((element) => consultaAtividade(elementoHTML, element.NUM_FLUXO, 1)); // Para cada Fluxo executa a Constulta de Atividades.
}

  //* Realiza consulta de Fluxos de Revisoes de Custos Ativos
Fluxos_Revisoes_Custos(RevisarConclusaoChamado_AtendimentoscomRevisaodeCustosnaoConcluida);

  //-------------------------------------------

  //Todo Preencher Select2 
function preencheDropDown(sendUrl, parentSelector, childSelector) {
    if ($(parentSelector).val() != "" && $(parentSelector).val() != null) {
        $(childSelector).children().remove();
        $.ajax(sendUrl, {
            method: "POST",
            data: { id: $(parentSelector).val() },
            success: function (response) {
                $(childSelector).append('<option value>' + campos.selecionar + '</option>');
                $(response).each(function (index, element) {
                    $(childSelector).append('<option value="' + element.Value + '">' + element.Text + '</option>');
                });
                $(childSelector).trigger("change");
            },
            error: function () { alertify.error(mensagens.ajaxErro); }
        });
    }
};


function f_alertCSS(){
    var v_widthTela = ($(this).width())/6
    $("#"+alertaAjuda).css({
        'width': v_widthTela,
        'display': 'none',
        'position': 'absolute',
        'background-color':'#fff',
        'border': '0.5px solid #7E7E7E',
        'height': 'auto',
        'padding': '0',
    })
}

function f_checkbox(){
    if(GetValue(checkbox) == true){
        SetVisible(prazoAvaliacaoPortal, true)
        SetRequired(prazoAvaliacaoPortal, true)
        var data = GetDate().split("/");
        var data_string = data[2] +'-'+ data[1] +'-'+ data[0];
        var v_prazoData = DateAddDays(data_string,GetValue(prazoParametroPortal),false)
        var data_split = v_prazoData.substring(0, 10).split("-");
        var parcela_string = data_split[2] +'/'+ data_split[1] +'/'+ data_split[0];
        SetValue(prazoAvaliacaoPortal, parcela_string)
    
    }else{
        SetVisible(prazoAvaliacaoPortal, false)
        SetRequired(prazoAvaliacaoPortal, false)
        SetValue(prazoAvaliacaoPortal, '')
    }
}

function f_chamadoContinuacao(){
    SetVisible(alertContinuacao, GetValue(chamadoContinuacao) == 'true')
}


//? ----------------------------------------------------------------------------------------------------------
//? FUNÇÕES PARA MOSTRAR AJUDA DE CAMPO COM MOUSE OVER NA (?)
//? ----------------------------------------------------------------------------------------------------------
//*Mostra a ajuda do campo com mouse over, sem precisar clicar na (?)
//*Precisa de um componente alert no formulário com alias 'alertaAjuda'. Pode estar dentro de um form reusável e ser configurado como invisivel, 
//*porém o form precisa estar numa área visível do form para funcionar
//*a função f_hoverAjuda() precisa ser chamada dentro de um Promise, para que execute somente ao final do carregamento da página. Abaixo há um 
//*exemplo de função com Promise 'f_rodarDepois()'. A 'f_rodarDepois()' deve rodar no loading do form.

function f_hoverAjuda() {
    function f_alertCSS(){
        var v_widthTela = ($(this).width())/6
        $("#"+alertaAjuda).css({
            'width': v_widthTela,
            'display': 'none',
            'position': 'absolute',
            'background-color':'#fff',
            'border': '0.5px solid #7E7E7E',
            'height': 'auto',
            'padding': '0',
        })
    }

    f_alertCSS()

    //var v_data = ('*[data-desatributo = "'+atual+'"]')
    $('.form-button-help').hover(function(e) {
        var v_data = $(this).data('desatributo') 
        $(".form-button-help").removeAttr("title");   
        var v_desAjuda = SqlQuery(`
            SELECT DES_HELP help
            FROM PCPJ_ENTIDADE_ATRIBUTOS EA
                ,PCPJ_ENTIDADE E
            WHERE E.COD_ENTIDADE = EA.COD_ENTIDADE
            AND EA.DES_FONTE_ATRIBUTO = '${v_data}'
            AND E.DES_ENTIDADE_ORIGEM = 'CL_HD_HELP_DESK'
        `)
        if(v_desAjuda !== null && v_desAjuda.entity !== null && v_desAjuda.entity.length >0){
            var v_textoAjuda = v_desAjuda.entity[0].help
        
            $("#"+alertaAjuda).html(`
            <table style="margin: 0 0% 0px 0%; width: 100%; ">
            <tbody>
                <tr>
                    <td style="text-align: left;height:40px;background-color: #eee;border-bottom:1px solid #babcbc ">
                        <p style="margin: 0 0 0 0;;color: #2e282a;"><span style="letter-spacing: -1px;font-size: 14px;">&nbsp;&nbsp;&nbsp;Ajuda</span></p>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#fff">
                    
                        <table style="width: 100%; background-color:#fff; font-family:Arial, Helvetica, sans-serif; letter-spacing: -1px;">
                            <tbody>
                                <tr>
                                    <td style="text-align: left;">
                                        <p style="margin: 5% 5% 5% 5%;color: #2e282a"><span style="letter-spacing: -1px;font-size: 14px">${v_textoAjuda}</span></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
            `)
        }  
        $('#'+alertaAjuda).show()
    // .css('top', e.pageY + moveDown)
        //.css('left', e.pageX + moveLeft)
    //.appendTo('body');
    }, function() {
        $('#'+alertaAjuda).hide();
    });
    $('.form-button-help').mousemove(function(e) {
        var v_tela = document.querySelector('#my-tab-content').offsetHeight
        var moveLeft = -120;
        var moveDown = -(v_tela + 250);
        $('#'+alertaAjuda).css('top', e.pageY + moveDown).css('left', e.pageX + moveLeft);
    });
};

function f_rodarDepois(){
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(
                f_hoverAjuda()
            )
        },1000)
    })
}

f_rodarDepois()
