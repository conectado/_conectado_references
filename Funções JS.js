//? ----------------------------------------------------------------------------------------------------------
//? FUNÇÕES DE VALOR DE CAMPO
//? ----------------------------------------------------------------------------------------------------------

//Todo GetValue
    //Pegar conteudo de campo
    GetValue(alias)
    //alias = alias do campo ou id do campo

//Todo SetValue 
    //Definir conteúdo de um Campo
    SetValue(alias, 'valor')
    //alias = alias do campo ou id do campo
    //valor = conteúdo a ser definido no campo

//Todo HasValue
    //Verificar se Campo tem Conteúdo. Retorna true ou false
    HasValue(alias)
    //alias = alias do campo ou id do campo

    //Exemplos:
    if (HasValue(alias)){
        'Tem Valor'
    }else{
        'Nao tem valor'
    }
    if (!HasValue(alias)){
        'Não Tem Valor'
    }else{
        'tem valor'
    }

//Todo OnChange
    //Pegar mudança (change) de campo e executar algum código
    OnChange(alias, function(){
        //código aqui
    });

//? ----------------------------------------------------------------------------------------------------------
//? FUNÇÕES DE PROPRIEDADE DE CAMPO
//? ----------------------------------------------------------------------------------------------------------

//Todo SetVisible 
    //Tornar campo Visível ou Não Visível*/
    SetVisible(alias , boolean)
    //alias = alias do campo ou id do campo
    //boolean = true para visíble e false para invisível

//Todo IsVisible
    //Testar se Campo é Visível. Retorna true ou false.
    IsVisible(alias)
    //alias = alias do campo ou id do campo

//Todo SetEditable
    //Tornar campo Editável ou Não Editável
    //? Campos não eidtáveis NÃO GRAVAM dados no banco
    SetEditable(alias , boolean )
    //alias = alias do campo ou id do campo
    //boolean = true para editável e false para não editável

//Todo SetReadOnly
    //Tornar Campo Editavel true ou false. GRAVA dados no base */
    SetReadOnly(alias,boolean)
    //alias = alias do campo ou id do campo
    //boolean = true para não editável e false para editável

//Todo SetRequired
    //Tornar campo Obrigatório ou Não Obrigatório       
    SetRequired(alias , boolean )
    //alias = alias do campo ou id do campo
    //boolean = true para obrigatório e false para opcional

//? ----------------------------------------------------------------------------------------------------------
//? FUNÇÕES DE DATA E HORA
//? ----------------------------------------------------------------------------------------------------------

//Todo GetDate
    //Formato string 'dd/MM/yyyy'
    GetDate() //pega da estação
    GetServerDate(); //pega do server

//Todo GetDateTime    
    //Formato string 'dd/MM/yyyy HH:mm:ss'
    GetDateTime() //pega da estação
    GetServerDateTime();//pega do server 

//Todo DateAddMinutes
    //Somar minutos em data usando calendario e horário da organização
    //Espera data no formato 'yyyy-MM-dd HH:mm:ss.fff'
    DateAddMinutes(data,minutos,usa_calendario);
    //data = data no formato 'yyyy-MM-dd HH:mm:ss.fff'
    //minutos = minutos (inteiro) a serem somados ou diminuidos. Para diminuir passar com sinal negativo (-).
    //usa_calendario = true ou false. Determina se deve considerar o calendario e os horários da organização.

        //Ex.: 
        var ret = DateAddMinutes('2018-10-31 17:50:20.437', 20,true); //considera  calendario e horario da empresa
        //Ex.: 
        var ret = DateAddMinutes('2018-10-31 17:50:20.437', 20,false); //não considera  calendario ou horario da empresa

//Todo DateAddDays        
    //Somar dias em data usando calendario e horário da organização
    //Espera data no formato 'yyyy-MM-dd HH:mm:ss.fff'
    DateAddDays(data,dias,usa_calendario);
    //data = data no formato 'yyyy-MM-dd HH:mm:ss.fff'
    //dias = dias (inteiro) a serem somados ou diminuidos. Para diminuir passar com sinal negativo (-).
    //usa_calendario = true ou false. Determina se deve considerar o calendario e os horários da organização.

        //Ex.: 
        var ret = DateAddDays('2018-10-31 17:50:20.437', 1,true); //considera  calendario e horario da empresa
        //Ex.: 
        var ret = DateAddDays('2018-10-31 17:50:20.437', 1,false); //não considera  calendario ou horario da empresa

//Todo DateDiff
    //Calcular diferença entre datas de acordo com datepart
    DateDiff(datepart, data_inicio, data_fim);
    //datepart = parte de data a ser calculada a diferença
        //dd = dia
        //min = minuto
        //hh = hora
    //data_inicio = data de inicio para comparação. Formato 'yyyy-MM-dd'
    //data_fim = data de fim para comparação. Formato 'yyyy-MM-dd'

        //Ex.: 
        var ret = DateDiff('dd', '2018-10-30', '2018-10-31');

//Todo ConvertStringToDate
    //Retorna data string no formato date "Fri Jul 08 2022 00:00:00 GMT-0300 (Horário Padrão de Brasília)"
    ConvertStringToDate('data')
    //'data' = informar data string

//Todo DateCompare
    //Compara duas datas e retorna:
        // 1 quando data a > data b
        //-1 quando data a < data b
        // 0 quando iguais
    DateCompare(data_inicio, data_fim)
    //data_inicio = data de inicio para comparação. Formato 'yyyy-MM-dd'
    //data_fim = data de fim para comparação. Formato 'yyyy-MM-dd'

    //Ex:
    if (DateCompare(data_inicio, data_fim) == 1  ){
        //código aqui
    }
    
//Todo SetMinMaxDateOfCalendar
    //DatePicker Max e Min Date
    SetMinMaxDateOfCalendar(alias,'minDate','maxDate','default');
    //alias = é o campo data do formulário,
    //'minDate' = data mínima admitida. Pode ser fixa no formato "YYYY-MM-DD' ou um inteiro  -1 (hoje+1), 0 (dia de hoje), 1 (hoje -1)
    //'maxDate' = data máxima permitida. Pode ser fixa no formato "YYYY-MM-DD' ou um inteiro  -1 (hoje+1), 0 (dia de hoje), 1 (hoje -1)
    //'default' = data default a fixa ao abrir o datapicker que deve er em formato string "DD/MM/YYYY"

    //Ex.:
        //Setando apenas a data Maxima 								
        SetMinMaxDateOfCalendar(alias,null,3,null);
        
        //Setando apenas a data Minima 								
        SetMinMaxDateOfCalendar(alias,2,null,null);
        
        //Setando Maxima e Minima 								
        SetMinMaxDateOfCalendar(alias,2,3,null);
        
        //Informando uma data base 						
        SetMinMaxDateOfCalendar(alias,2,3,"10/06/2019");
        
        //Outros exemplos
        SetMinMaxDateOfCalendar(alias,"2019-06-11","2019-06-30",null);
        
        SetMinMaxDateOfCalendar(alias,2,"2019-06-30",null);
        
        SetMinMaxDateOfCalendar(alias,null,"2019-06-30",null);

//? ----------------------------------------------------------------------------------------------------------
//? FUNÇÕES DE TABELA (COLLECTION)
//? ----------------------------------------------------------------------------------------------------------

//Todo TableCountRows    
    //Conta linhas de uma tabela
    TableCountRows(alias)
    //alias = é o id ou alias da tabela no formulário,

    //Ex.:
	if(TableCountRows('20180108104611377') > 0){         
		//codigo aqui
	}

//Todo SumColumnTable
    // Somar coluna de tabela
    SumColumnTable(alias,num_coluna)
    //alias = id ou alias da tabela
    //num_coluna = número da coluna, começando em 1

    //Ex.:
    var valor = SumColumnTable('20171225211927347',5)

//Todo NewRecord
	//Criar registro novo na tabela (coleção)
	NewRecord(alias)
    //alias = id ou alias da tabela
    
    //Ex.:
        var novo = NewRecord(alias)

//Todo SetValue (em NewRecord)
	//Definir valor no novo registro
    .SetValue('caminho virtual','valor')
    //'caminho virtual' = caminho virtual do campo a ser definido tomando a tabela como base
    //'valor' = valor a ser definido

    //Ex.:
        novo.SetValue('CL_LOG_AVALIACOES.idCL_CHAMADO','Ola ...')

//Todo Delete (em NewRecord)
	//Deletar Registro Criado em Tabela. Registro precisa estar numa variável.
	.Delete()
    //não possui parametros

    //Ex.:
    novo.Delete()

//Todo DeleteAllRecords
	//Deletar todos os registros da Tabela
	DeleteAllRecords(alias)
    //alias = id ou alias da tabela

//Todo GetRecord
    //Pegar registro de uma tabela com base no numero da linha
    GetRecord(alias, num_linha)
    //alias = id ou alias da tabela
    //num_linha = número da linha, começando em 1

    //Ex.:
	var obj = GetRecord('20171225211927347',1)

//Todo GetValue (em GetRecord)
    //Pegar valor de campo do registro. O registro da tabela (linha) precisa estar em uma variável.
    .GetValue('caminho virutal')
    //'caminho virtual' = caminho virtual do campo a ser definido tomando a tabela como base

    //Ex.:
    obj.GetValue('CL_LISTA_DE_CONVIDADOS.DES_NOME')
    
//Todo SetValue (em GetRecord)
    //Definir valor em campo do registro. O registro da tabela (linha) precisa estar em uma variável.
    .SetValue('caminho virtual','valor', 'valor_exibição (opcional)', 'arquivos (opicional)')
    //'caminho virtual' = caminho virtual do campo a ser definido tomando a tabela como base
    //'valor' = valor a ser definido
    //'valor_exibição (opcional)' = valor a ser exibido, necessário quando o campo definido é tipo Entity
    //'arquivos (opcional)' = usado para copiar arquivo de campo de fomulário para tabela

    //Ex.:
    obj.SetValue('CL_LISTA_DE_CONVIDADOS.DES_NOME','Maria')
    
    // Definindo valor em campo tipo Entity de tabela
    obj.SetValue('CL_LISTA_DE_CONVIDADOS.COD_PROFISSIONAL',24,'João Pedro')

    // Copiar arquivo de campo de formulário para 
	var files = GetFiles('20171225211927347')
	obj.SetValue('CL_LISTA_DE_CONVIDADOS.DES_ANEXOS','', '' , files)

//Todo SetEditable (em GetRecord)
    // Oculta o botão de edição da linha da tabela. O registro da tabela (linha) precisa estar em uma variável.
    .SetEditable(boolean)
    //boolean = true para visíble e false para invisível

    //Ex.:
    obj.SetEditable(false)

//Todo SetDeletable (em GetRecord)
    // Oculta o botão de exclusão da linha da tabela. O registro da tabela (linha) precisa estar em uma variável.    
    .SetDeletable(boolean)
    //boolean = true para visíble e false para invisível

    //Ex.:
    obj.SetDeletable(false)

//Todo NewEntityRecord
    //Salvar registro diretamente no banco, sem precisar de ação do usuário
	//Criar registro direto no Banco 
	NewEntityRecord('tabela')
    //'tabela' = nome da tabela no banco de dados

    //Ex.: 
    var novo = NewEntityRecord('CL_CHAMADOS')

//Todo SetValue (em NewEntityRecord)
	//Definir valor no registro. O registro da tabela (linha) precisa estar em uma variável.   
	.SetValue('campo', 'valor');
    //'campo' = nome do campo no banco de dados a ser definido
    //'valor' = valor a ser definido

    //Ex.:
    novo.SetValue('DES_CHAMADO', 'Lorem ipsum...')

//Todo SaveChanges (em NewEntityRecord)
    //Salvar registro no banco. O registro da tabela (linha) precisa estar em uma variável. 
	.SaveChanges(boolean /*opcional*/)
    //boolean = true ou false. Quando true executa as ações de salvamento da atividade

    //Ex.:
    novo.SaveChanges() // não executa as ações de salvamento da atividade
	novo.SaveChanges(true) // executa as ações de salvamento da atividade

//Todo GetValue (em NewEntityRecord)
	// Pegar valor de campos em registro novo. Pode ser usado para pegar o id do registro recem criado
    .GetValue('campo')
	//'campo' = nome do campo no banco de dados a ser buscado valor

    //Ex.:
    var id = novo.GetValue('idCL_CHAMADO') // Pega id do registro criado na tabela

//Todo TableOnUpdate
    //Executar código no evento de alteração de tabela
    TableOnUpdate(alias , function(){
        //código aqui
    })
    //alias = id ou alias da tabela

//? ----------------------------------------------------------------------------------------------------------
//? FUNÇÕES DE ARQUIVOS (ANEXOS)
//? ----------------------------------------------------------------------------------------------------------

//Todo GetFiles
	// retorna todos os arquivos de um campo do tipo anexo (arquivo)
    GetFiles(alias)
    //alias = 'id' ou alias do campo

    //Ex.:
	var files = GetFiles(aquivo) // não funciona em form de tabela*/

//Todo Rename
	// renomear arquivo. O campo anexo precisa estar em uma variável. 
	.Rename('nome_atual_arquivo' , 'novo_nome_arquivo')
    //nome_atual_arquivo = nome atual do arquivo com extensão
    //novo_nome_arquivo = nome novo do arquivo com extensão

    //Ex.:
    files.Rename('arquivo.txt' , 'novoarquivo.txt')

//Todo DeleteFile
	// Deleta arquivo pelo nome. O campo anexo precisa estar em uma variável. 
    .DeleteFile('nome_arquivo')
    //nome_arquivo = nome do arquivo com extensão a ser deletado
    //Ex.:
	files.DeleteFile('arquivo.txt')

//Todo DeleteAll
	// Deleta todos arquivo do campo anexo. O campo anexo precisa estar em uma variável. 
	.DeleteAll()
    //Ex.:
    files.DeleteAll()

//Todo CopyFileTo
	// Copia arquivo pelo nome para outro campo tipo Arquivo
    .CopyFileTo(alias, 'nome_arquivo')
    //alias = 'id' ou alias do campo
    //nome_arquivo = nome do arquivo com extensão a ser copiado

    //Ex.:
	files.CopyFileTo('201804021559090999', 'arquivo.txt')

//Todo CopyAllFileTo
	// Copia todos os arquivos do campo anexo
    .CopyAllFileTo(alias)
    //alias = 'id' ou alias do campo

    //Ex.:
	files.CopyAllFileTo('201804021559090999');

//Todo HasFile
    //Verificar se Campo tem Arquivos anexados. Retorna true ou false
	HasFile(alias) ;
    //alias = 'id' ou alias do campo

//Todo CountFiles
    //Retorna quantidade de Arquivos anexados.
	CountFile(alias)
    //alias = 'id' ou alias do campo


//? ----------------------------------------------------------------------------------------------------------
//? FUNÇÕES PARA COMPONENTES
//? ----------------------------------------------------------------------------------------------------------

//Todo ShowTab
	// Escolher/selecionar Aba
	ShowTab(alias)
    //alias = 'id' ou alias da aba

//Todo PanelExpanded
    //Expandir ou recolher Painel (panel)
	PanelExpanded(alias, boolean)
    //alias = alias do campo ou id do campo
    //boolean = true para expandido e false para retraído

    //Ex.:
    PanelExpanded('20180405155157667', true)

//Todo GetTableQuery
    // Colocar TableQuery em variável
    GetTableQuery(alias)
    //alias = 'id' ou alias da TableQuery

    //Ex.:
    var a = GetTableQuery(alias)

//Todo .SetParameter
    // Definir valor de variável da consulta do TableQuery. A TableQuery precisa estar em uma variável. 
    .SetParameter('variavel', 'valor')
    //'variavel' = variavel da TableQuery a ser definido
    //'valor' = valor a ser passado para a variável

    //Ex.:
    a.SetParameter('@reg', 5)

//Todo .Refresh
    //Atualizar TableQuery após passar valor de variável. A TableQuery precisa estar em uma variável. 
    .Refresh()
    //não possui parametros.

    //Ex.:
    a.Refresh()

//? ----------------------------------------------------------------------------------------------------------
//? FUNÇÕES DIVERSAS
//? ----------------------------------------------------------------------------------------------------------

//Todo Validação de Formulário
    //Validar conteúdo de formulário pemitindo concluir a atividade ou mostrar mensagem de erro/alerta para o usuário
    ValidationMessage('mensagem','boolean (opcional)')
    //'mensagem' = mensagem a ser mostrada. A mensagem impede a finalização da atividade.
    //'boolean (opcional)' = true ou false para determinar refresh da página após. 
    //Parametro opicional, quando true força refresh na página após clicar em ok. 
    
    //Ex.:
    if(teste == true){
        ValidationMessage('Lorem ipsum...')
    }
    if(teste2 == true){
        ValidationMessage('Lorem ipsum...',true)
    }

//Todo GetWorkFlowInfo
    //Informações da instancia informada
    GetWorkFlowInfo(fluxo)
    //fluxo = número do fluxo/instancia que deseja consultar

    //*informações disponíveis 
        .numero // número da instancia
        .codStatus // código do status da instancia
        .desStatus // descrição do status da instancia
        .dtaCriacao // data de criação instancia
        .dtaEncerramento // data de encerrametno da instancia
        .codRegistroEntidade // id do registro da tabela base do processo

    //Ex.:
    var info = GetWorkFlowInfo(1907);
        info.numero
        info.codStatus
        info.desStatus
        info.dtaCriacao
        info.dtaEncerramento
        info.codRegistroEntidade

//Todo GetThisWorkFlowInfo
    //Informações da instancia atual
    GetThisWorkFlowInfo()
    //não tem parametro

    //*informações disponíveis 
        .numero // número da instancia
        .codStatus // código do status da instancia
        .desStatus // descrição do status da instancia
        .dtaCriacao // data de criação instancia
        .dtaEncerramento // data de encerrametno da instancia
        .codRegistroEntidade // id do registro da tabela base do processo

    //Ex.:
    var info = GetThisWorkFlowInfo();
        info.numero;
        info.codStatus;
        info.desStatus;
        info.dtaCriacao;
        info.dtaEncerramento;
        info.codRegistroEntidade;

 //Todo GetThisTaskInfo       
    //Informações da Atividade Atual, constantes na PCPJ_EXECUCAO_ATIVIDADE, tabela que armazena cada 
    //instancia de atividade criada em execução
    GetThisTaskInfo()
    //não tem parametro

    //*informações disponíveis
        .codAtividadeExecucao //id da atividade
        .dtaPrevistaFim // data de prazo da atividade
        .codInstancia // numero do fluxo / instancia
        .codStatus // código do status da atividade
        .desStatus // descrição do status da atividade
        .dtaCriacao  // data de criação da atividade
        .numDuracaoAtividade  // prazo de duração da atividade em minutos
        .numTempoRisco // prazo de tempo de risco da atividade em minutos
        .dtaEncerramento  //data de encerramento da atividade
        .dtaApropriacao //data apropriação da atividade
        .desFantasia // nome da atividade

    //Ex.:
    var dados = GetThisTaskInfo()

            dados.codAtividadeExecucao
            dados.dtaPrevistaFim;
            dados.codInstancia
            dados.codStatus 
            dados.desStatus 
            dados.dtaCriacao 
            dados.numDuracaoAtividade 
            dados.numTempoRisco 
            dados.dtaEncerramento 
            dados.dtaApropriacao 
            dados.desFantasia

//Todo GetUserInfo
	//Buscar dados do usuário logado na atividade
    GetUserInfo()
    //não tem parametro

    //*informações disponíveis
        .id // código do usuário no sistema
        .desNome //nome completo do usuário
        .desApelido //apelido do usuário

    //Ex.:
	var usuario = GetUserInfo();
        usuario.id
        usuario.desNome
        usuario.desApelido

//Todo ApplyFilter
    //Aplicar Filtro em campo Combo (Select2) e tabelas*/
    ApplyFilter(alias,'condicao')
    //alias = 'id' ou alias do campo
    //'condição' = clausula WHERE para filtrar o elemento
    
    //Ex.:
        ApplyFilter(select2,"DES_NOME like '%a%'")
        ApplyFilter(tabela,'COD_USUARIO in (SELECT COD_USUARIO FROM PCPJ_USUARIO_PAPEL WHERE COD_PAPEL = 3 )')

//? ----------------------------------------------------------------------------------------------------------
//? FUNÇÕES DE ACESSO A BANCO DE DADOS
//? ----------------------------------------------------------------------------------------------------------

//Todo SqlQuery 
    //Fazer consulta SQL no JS
    SqlQuery('sentenca')
    //'sentenca' = sentenca sql

    //Ex.:
    var ret = SqlQuery('select * from CL_CRIACAO_TURMAS')
    if(ret !== null && ret.entity !== null && ret.entity.length > 0){
        ret.entity[0].idCL_CRIACAO_TURMAS
    }

//Todo GetEntity  
    //Função para pegar conteúdo de entidade(apenas entidades que constam na PCPJ_ENTIDADES)
    GetEntity('nomeEntidadeBanco','condicao')
    //'nomeEntidadeBanco' = nome da tabela no banco de dados
    //'condicao' = clausula WHERE para filtrar o elemento

    //Ex.:
        var entidade = GetEntity('PCPJ_USUARIOS', 'COD_USUARIO = 175');
        if(entidade !== null && entidade.entity !== null && entidade.entity.length > 0){
            entidade.entity[0].DES_NOME_COMPLETO // usar nome de campo de banco
        }

//Todo SetEntity
    //Função para alterar conteúdo de entidade(apenas entidades que constam na PCPJ_ENTIDADES)
    SetEntity('nomeEntidadeBanco', 'idRegistro', 'nomeCampoBanco','valor')
    //'nomeEntidadeBanco' = nome da tabela no banco de dados
    //'idRegistro' = id do registro a ser alterado
    //'nomeCampoBanco' = campo do banco de dados
    //'valor' = valor a ser definido no campo